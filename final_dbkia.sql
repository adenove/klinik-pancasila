-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Waktu pembuatan: 19 Agu 2019 pada 07.06
-- Versi server: 5.7.23
-- Versi PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbbkia1`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `biaya_lain`
--

CREATE TABLE `biaya_lain` (
  `id_biaya_lain` int(11) NOT NULL,
  `id_kunjungan` int(11) NOT NULL,
  `jenis_layanan` varchar(50) NOT NULL,
  `biaya_layanan` int(6) NOT NULL,
  `jumlah_layanan` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `biaya_lain`
--

INSERT INTO `biaya_lain` (`id_biaya_lain`, `id_kunjungan`, `jenis_layanan`, `biaya_layanan`, `jumlah_layanan`) VALUES
(1, 3, 'Suntik', 15000, 1),
(2, 3, 'Periksa Lab', 20000, 1),
(3, 11, 'jasa periksa', 20000, 1),
(4, 10, 'jasa periksa', 20000, 1),
(5, 10, 'Alkes', 15000, 1),
(6, 17, 'jas dokter', 1000, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_pasien`
--

CREATE TABLE `data_pasien` (
  `id` int(11) NOT NULL,
  `id_pasien` varchar(1000) NOT NULL,
  `tanggal` varchar(20) NOT NULL,
  `subyektif_objectif` varchar(500) NOT NULL,
  `assesment` varchar(150) NOT NULL,
  `Pemeriksaan_penunjang` varchar(500) NOT NULL,
  `planning` varchar(500) NOT NULL,
  `paraf` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `data_pasien`
--

INSERT INTO `data_pasien` (`id`, `id_pasien`, `tanggal`, `subyektif_objectif`, `assesment`, `Pemeriksaan_penunjang`, `planning`, `paraf`) VALUES
(17, '20', '06/04/2019', 'detail', 'jantung', 'sakit', 'minum obat', 'hampir mati');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dokter`
--

CREATE TABLE `dokter` (
  `id_dokter` int(11) NOT NULL,
  `nama_dokter` varchar(100) NOT NULL,
  `username_dokter` varchar(45) NOT NULL,
  `password_dokter` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dokter`
--

INSERT INTO `dokter` (`id_dokter`, `nama_dokter`, `username_dokter`, `password_dokter`) VALUES
(1, 'dr. Rina', 'rina', '202cb962ac59075b964b07152d234b70'),
(2, 'dr. Gisha', 'gisha', '202cb962ac59075b964b07152d234b70'),
(3, 'dr. Fendrik', 'Fendrik', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Struktur dari tabel `icd_diagnosa`
--

CREATE TABLE `icd_diagnosa` (
  `id` int(11) NOT NULL,
  `icd` varchar(200) NOT NULL,
  `diagnosa` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `icd_diagnosa`
--

INSERT INTO `icd_diagnosa` (`id`, `icd`, `diagnosa`) VALUES
(102, 'A00', 'Kolera'),
(103, 'A00.0', 'Cholera due to Vibrio cholerae 01, biovar cholerae'),
(104, 'A00.1', 'Cholera due to Vibrio cholerae 01, biovar eltor.'),
(105, 'A00.9', 'Cholera, unspecified'),
(106, 'A01', 'Demam tifoid dan paratifoid'),
(107, 'A01.0', 'Demam tifoid'),
(108, 'A01.1', 'Demam paratifoid A'),
(109, 'A01.2', 'Demam paratifoid B'),
(110, 'A01.3', 'Demam paratifoid C'),
(111, 'A01.4', 'Demam paratifoid, YTT'),
(112, 'A02', 'Infeksi salmonella lainnya'),
(113, 'A02.0', 'Salmonella enteritis'),
(114, 'A02.1', 'Salmonella septicaemia'),
(115, 'A02.2', 'Localized salmonella infections'),
(116, 'A02.8', 'Other specified salmonella infections'),
(117, 'A02.9', 'Salmonella infection, unspecified'),
(118, 'A03', 'Sigelosis'),
(119, 'A03.0', 'Shigellosis due to Shigella dysenteriae'),
(120, 'A03.1', 'Shigellosis due to Shigella flexneri'),
(121, 'A03.2', 'Shigellosis due to Shigella boydii'),
(122, 'A03.3', 'Shigellosis due to Shigella sonnei'),
(123, 'A03.8', 'Other shigellosis'),
(124, 'A03.9', 'Shigellosis, unspecified'),
(125, 'A04', 'Infeksi bakteri usus lainnya'),
(126, 'A04.0', 'Enteropathogenic Escherichia coli infection'),
(127, 'A04.1', 'Enterotoxigenic Escherichia coli infection'),
(128, 'A04.2', 'Enteroinvasive Escherichia coli infection'),
(129, 'A04.3', 'Enterohaemorrhagic Escherichia coli infection'),
(130, 'A04.4', 'Other intestinal Escherichia coli infections'),
(131, 'A04.5', 'Campylobacter enteritis'),
(132, 'A04.6', 'Enteritis due to Yersinia enterocolitica'),
(133, 'A04.7', 'Enterocolitis due to Clostridium difficile'),
(134, 'A04.8', 'Other specified bacterial intestinal infections'),
(135, 'A04.9', 'Bacterial intestinal infection, unspecified'),
(136, 'A05', 'Intoksikasi bakteri tular-makanan lainnya'),
(137, 'A05.0', 'Foodborne staphylococcal intoxication'),
(138, 'A05.1', 'Botulism'),
(139, 'A05.2', 'Foodborne Clostridium perfringens [Clostridium wel'),
(140, 'A05.3', 'Foodborne Vibrio parahaemolyticus intoxication'),
(141, 'A05.4', 'Foodborne Bacillus cereus intoxication'),
(142, 'A05.8', 'Other specified bacterial foodborne intoxications'),
(143, 'A05.9', 'Bacterial foodborne intoxication, unspecified'),
(144, 'A06', 'Amubiasis'),
(145, 'A06.0', 'Disentri amuba akut'),
(146, 'A06.1', 'Amubiasis intestin kronis'),
(147, 'A06.2', 'Kolitis amuba non-disentri'),
(148, 'A06.3', 'Amuboma intestin'),
(149, 'A06.4', 'Abses hati amuba'),
(150, 'A06.5', 'Abses paru amuba (J99.8*)'),
(151, 'A06.6', 'Abses otak amuba (G07*)'),
(152, 'A06.7', 'Amubiasis kulit'),
(153, 'A06.8', 'Infeksi amuba di tempat lainnya'),
(154, 'A06.9', 'Amubiasis, YTT'),
(155, 'A07', 'Penyakit protozoa usus lainnya'),
(156, 'A07.0', 'Balantidiasis'),
(157, 'A07.1', 'Giardiasis [lambliasis]'),
(158, 'A07.2', 'Cryptosporidiosis'),
(159, 'A07.3', 'Isosporiasis'),
(160, 'A07.8', 'Other specified protozoal intestinal diseases'),
(161, 'A07.9', 'Protozoal intestinal disease, unspecified'),
(162, 'A08', 'Infeksi virus usus dan lainnya'),
(163, 'A08.0', 'Rotaviral enteritis'),
(164, 'A08.1', 'Acute gastroenteropathy due to Norwalk agent'),
(165, 'A08.2', 'Adenoviral enteritis'),
(166, 'A08.3', 'Other viral enteritis'),
(167, 'A08.4', 'Viral intestinal infection, unspecified'),
(168, 'A08.5', 'Other specified intestinal infections'),
(169, 'A09', 'Other gastroentriris and colitis of infectious and unspecified origin'),
(170, 'A09.0', 'Orher and unspecified gastroentritis and colitis infectious'),
(171, 'A09.9', 'Gastroenteritis and colitis of unspecified origin'),
(172, 'A15', 'Tbc sistem napas, dengan konfirmasi bakteriologi &'),
(173, 'A15.0', 'Tuberkulosis paru, mikroskopis sputum, dengan/tanpa pemeriksaan kultur'),
(174, 'A15.1', 'Tuberkulosis paru, dengan konfirmasi biakan saja'),
(175, 'A15.2', 'Tuberkulosis paru, dengan konfirmasi histologi'),
(176, 'A15.3', 'Tuberkulosis paru, konfirmasi tidak dirinci'),
(177, 'A15.4', 'Tuberkulosis kelenjar getah bening intratoraks, de'),
(178, 'A15.5', 'Tuberkulosis laring, trakea & bronkus, dengan konf'),
(179, 'A15.6', 'Tuberkulosis pleura, dengan konfirmasi bakteriolog'),
(180, 'A15.7', 'Tuberkulosis sistem primer, dengan konfirmasi bakt'),
(181, 'A15.8', 'Tuberkulosis sistem lainnya, dengan konfirmasi bak'),
(182, 'A15.9', 'Tuberkulosis sistem napas YTT, dengan konfirmasi b'),
(183, 'A16', 'Tbc sistem napas, tanpa konfirmasi bakteriologi &'),
(184, 'A16.0', 'Tuberkulosis paru, bakteriologi dan histologi negatif'),
(185, 'A16.1', 'Tuberkulosis paru, bakteriologi dan histologi tidak diperiksa'),
(186, 'A16.2', 'Tuberkulosis paru tanpa'),
(187, 'A16.3', 'Tuberkulosis kelenjar getah bening intratoraks, ko'),
(188, 'A16.4', 'Tuberkulosis laring, trakea dan bronkus, konfirmas'),
(189, 'A16.5', 'Tuberkulosis pleura, konfirmasi tidak disebut'),
(190, 'A16.7', 'Tuberkulosis sistem napas primer, konfirmasi tidak'),
(191, 'A16.8', 'Tuberkulosis sistem napas lainnya, konfirmasi tidak'),
(192, 'A16.9', 'Tuberkulosis sistem napas YTT, konfirmasi tidak'),
(193, 'A17', 'Tuberkulosis sistem saraf'),
(194, 'A17.0', 'Tuberkulosis selaput otak (G01*)'),
(195, 'A17.1', 'Tuberkuloma selaput otak (G07.*)'),
(196, 'A17.8', 'Tuberkulosis lain sistem saraf'),
(197, 'A17.9', 'Tuberkulosis sistem saraf YTT (G99.8*)'),
(198, 'A18', 'Tuberkulosis Selain Paru (Extra Pulmoner)'),
(199, 'A18.0', 'Tuberkulosis tulang dan sendi'),
(200, 'A18.1', 'Tuberkulosis sistem kemih kelamin'),
(201, 'A18.2', 'Tuberkulosis kelenjar getah bening perifer');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kunjungan`
--

CREATE TABLE `kunjungan` (
  `id_kunjungan` int(11) NOT NULL,
  `tanggal_kunjungan` datetime NOT NULL,
  `jenis_kunjungan` varchar(1) NOT NULL,
  `keluhan` text NOT NULL,
  `id_pasien` varchar(6) NOT NULL,
  `id_dokter` int(11) NOT NULL,
  `kunjungan_status` enum('antre','resep','lunas','trash') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kunjungan`
--

INSERT INTO `kunjungan` (`id_kunjungan`, `tanggal_kunjungan`, `jenis_kunjungan`, `keluhan`, `id_pasien`, `id_dokter`, `kunjungan_status`) VALUES
(7, '2019-07-11 15:27:43', '4', 'sakit', '000001', 1, 'lunas'),
(8, '2019-07-12 15:07:44', '2', 'sakit pusing', '000002', 2, 'lunas'),
(9, '2019-07-12 18:27:34', '1', 'batuk pilek panas', '000003', 1, 'resep'),
(10, '2019-07-12 18:28:28', '1', 'batuk pilek panas', '000004', 1, 'lunas'),
(11, '2019-07-12 18:53:47', '1', 'Diare', '000004', 1, 'lunas'),
(12, '2019-07-07 00:00:00', '4', 'sakit', '000001', 1, 'resep'),
(14, '2019-07-12 00:00:00', '4', 'sakit', '000005', 3, 'resep'),
(16, '2019-07-13 11:46:09', '3', 'sakit raga', '000004', 2, 'resep'),
(17, '2019-07-13 11:58:38', '4', 'sakit', '000001', 1, 'resep'),
(18, '2019-07-15 22:03:41', '1', 'sakit', '000006', 2, 'antre'),
(19, '2019-07-21 14:43:55', '1', 'sakit', '000007', 2, 'antre'),
(20, '2019-07-27 20:58:17', '1', 'sakit raga', '000008', 1, 'antre'),
(21, '2019-07-29 14:49:14', '1', 'sakit raga', '000009', 1, 'lunas'),
(22, '2019-08-03 13:42:02', '3', 'sakit raga', '000010', 1, 'antre');

-- --------------------------------------------------------

--
-- Struktur dari tabel `obat`
--

CREATE TABLE `obat` (
  `id_obat` int(11) NOT NULL,
  `nama_obat` varchar(50) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `stok_obat` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `obat`
--

INSERT INTO `obat` (`id_obat`, `nama_obat`, `harga_jual`, `stok_obat`) VALUES
(1, 'Amoxicilin', 8000, 13),
(2, 'Clonidine', 5000, 9),
(3, 'Parasetamol', 4500, 14),
(4, 'Ketokonazol', 6000, 44),
(5, 'Kotrimoksazol', 5000, 10),
(6, 'Cisplatin', 4000, 0),
(7, 'Ambroksol', 7000, 0),
(8, 'Asam Mefenamat', 11000, 0),
(9, 'AXXX', 12000, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `obat_masuk`
--

CREATE TABLE `obat_masuk` (
  `id_obat_masuk` int(11) NOT NULL,
  `id_obat` int(11) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `jumlah_obat_masuk` int(6) NOT NULL,
  `tanggal_obat_masuk` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `obat_masuk`
--

INSERT INTO `obat_masuk` (`id_obat_masuk`, `id_obat`, `harga_beli`, `jumlah_obat_masuk`, `tanggal_obat_masuk`) VALUES
(1, 1, 3000, 10, '2019-06-12 22:00:47'),
(2, 2, 4000, 5, '2019-06-12 22:02:59'),
(3, 3, 2000, 10, '2019-06-14 11:35:48'),
(9, 1, 3000, 20, '2019-06-14 13:40:02'),
(10, 3, 3000, 5, '2019-06-14 15:35:53'),
(11, 4, 3000, 50, '2019-06-15 01:44:34'),
(12, 5, 6000, 10, '2019-06-15 01:46:08'),
(13, 9, 3000, 2, '2019-06-20 13:23:35'),
(14, 3, 3000, 10, '2019-07-12 18:38:21'),
(15, 1, 3000, 20, '2019-07-13 19:40:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pasien`
--

CREATE TABLE `pasien` (
  `id_pasien` varchar(6) NOT NULL,
  `nomor_rekam_medis` varchar(20) NOT NULL,
  `nama_pasien` varchar(100) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `usia` varchar(150) NOT NULL,
  `agama` varchar(1) NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `nama_orangtua` varchar(1000) NOT NULL,
  `alamat` text NOT NULL,
  `status` varchar(1) NOT NULL,
  `pendidikan` varchar(1) NOT NULL,
  `pekerjaan` varchar(2) NOT NULL,
  `jenis_pasien` varchar(1) NOT NULL,
  `provinsi` varchar(1000) NOT NULL,
  `kabupaten` varchar(1000) NOT NULL,
  `kecamatan` varchar(1000) NOT NULL,
  `kelurahan` varchar(200) NOT NULL,
  `tanggal_daftar` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pasien`
--

INSERT INTO `pasien` (`id_pasien`, `nomor_rekam_medis`, `nama_pasien`, `tanggal_lahir`, `usia`, `agama`, `jenis_kelamin`, `nama_orangtua`, `alamat`, `status`, `pendidikan`, `pekerjaan`, `jenis_pasien`, `provinsi`, `kabupaten`, `kecamatan`, `kelurahan`, `tanggal_daftar`) VALUES
('000001', '000001', 'hendra', '2010-02-09', '9 tahun', '1', 'L', '', 'praci', '2', '3', '1', '1', 'Kalimantan Timur', 'Berau', 'Biatan', '', '2019-05-17 21:35:49'),
('000002', '000002', 'hendri', '2017-05-17', '2 tahun', '2', 'L', '', 'semanu', '2', '2', '2', '2', 'Nanggroe Aceh Darussalam (NAD)', 'Aceh Besar', 'wonosari', '', '2019-07-17 21:35:49'),
('000003', '000003', 'Tina Arista', '1994-07-30', '24 tahun', '1', 'P', '', 'Danan, Sendangagung', '2', '3', '7', '1', 'Jawa Tengah', 'Wonogiri', 'Baturetno', '', '2019-07-17 21:35:49'),
('000004', '000004', 'Arifin', '1994-07-30', '24 tahun', '1', 'L', '', 'Ngepoh Lor, Balepanjang', '2', '3', '7', '1', 'Jawa Tengah', 'Wonogiri', 'Baturetno', '', '2019-07-17 21:35:49'),
('000005', '000005', 'eko emon', '2018-10-01', '9 bulan', '1', 'L', '', 'bedoyo', '2', '3', '5', '2', 'Sumatera Barat', 'Dharmasraya', 'yogya', '', '2019-07-17 21:35:49'),
('000006', '000006', 'hayyo', '2018-01-01', '1 tahun', '1', 'L', '', 'sapit', '2', '2', '1', '1', 'DI Yogyakarta', 'Gunung Kidul', 'Semanu', '', '2019-07-17 21:35:49'),
('000007', '000007', 'ronaldo', '2013-01-01', '6 tahun', '4', 'L', '', 'praci', '1', '3', '1', '2', 'Jawa Tengah', 'Wonogiri', 'Pracimantoro', '', '2019-07-21 14:43:55'),
('000008', '000008', 'darwin', '2017-10-03', '1 tahun', '4', 'L', 'parjo sukijan', 'ngabean', '2', '2', '1', '1', 'Nanggroe Aceh Darussalam (NAD)', 'Aceh Barat', 'Arongan Lambalek', '', '2019-07-27 20:58:17'),
('000009', '000009', 'eko emon', '2017-08-28', '1 tahun', '3', 'L', 'parjo sukirman', 'serut', '2', '2', '1', '1', 'DI Yogyakarta', 'Gunung Kidul', 'Purwosari', 'Giriasih', '2019-07-29 14:49:14'),
('000010', '000010', 'test', '2017-05-01', '2 tahun', '4', 'L', 'parjo sukirman', 'bedoyo', '1', '1', '1', '1', 'DI Yogyakarta', 'Kulon Progo', 'Wates', 'Giri Peni', '2019-08-03 13:42:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `tanggal_pembayaran` datetime NOT NULL,
  `total_pembayaran` int(8) NOT NULL,
  `id_kunjungan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembayaran`
--

INSERT INTO `pembayaran` (`id_pembayaran`, `tanggal_pembayaran`, `total_pembayaran`, `id_kunjungan`) VALUES
(1, '2019-07-12 13:34:40', 16000, 7),
(2, '2019-07-12 18:38:54', 0, 8),
(3, '2019-07-12 19:00:00', 32500, 11),
(4, '2019-07-12 19:17:10', 43000, 10),
(5, '2019-07-29 15:07:20', 40000, 21);

-- --------------------------------------------------------

--
-- Struktur dari tabel `periksa`
--

CREATE TABLE `periksa` (
  `id_periksa` int(11) NOT NULL,
  `id_kunjungan` int(11) NOT NULL,
  `icd` text NOT NULL,
  `diagnosa` text NOT NULL,
  `tindakan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `periksa`
--

INSERT INTO `periksa` (`id_periksa`, `id_kunjungan`, `icd`, `diagnosa`, `tindakan`) VALUES
(1, 3, '23556', 'Tipes', 'Suntik'),
(2, 6, '23577', 'Tipes', 'Suntik'),
(3, 1, '57183', 'gawat', 'operasi'),
(4, 7, '69704', 'gawat', 'operasi'),
(5, 8, '32575', 'acute upper respiratory', 'tidak ada'),
(6, 11, '17602', 'Dermatitis', 'tidak ada'),
(7, 10, '54356', 'Dermatitis', 'tidak ada'),
(8, 9, '32257', 'gawat', 'operasi'),
(9, 17, '56241', 'gawat', 'operasi'),
(10, 12, '23094', 'gawat', 'operasi'),
(11, 14, '23401', 'gawat', 'operasi'),
(12, 21, '1234', 'gawat', 'operasi'),
(13, 16, 'A00.1', 'Cholera due to Vibrio cholerae 01, biovar eltor.', 'operasi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `resep`
--

CREATE TABLE `resep` (
  `id_resep` int(11) NOT NULL,
  `id_kunjungan` int(11) NOT NULL,
  `id_obat` int(11) NOT NULL,
  `jumlah_obat_keluar` int(6) NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `resep`
--

INSERT INTO `resep` (`id_resep`, `id_kunjungan`, `id_obat`, `jumlah_obat_keluar`, `keterangan`, `tanggal`) VALUES
(15, 7, 1, 2, '3x1 ', '2019-07-15 23:03:47'),
(16, 9, 1, 1, '3 x 1', '2019-07-15 23:03:47'),
(17, 11, 1, 1, '3 x 1', '2019-07-15 23:03:47'),
(18, 11, 3, 1, '3 x 1', '2019-07-15 23:03:47'),
(19, 10, 1, 1, '3 x 1', '2019-07-15 23:03:47'),
(20, 17, 1, 2, '3x1 ', '2019-07-15 23:03:47'),
(21, 17, 2, 2, '3x1', '2019-07-15 23:03:47'),
(22, 12, 3, 5, '3x1 ', '2019-07-15 23:03:47'),
(23, 14, 1, 5, '2x1', '2019-07-15 23:03:47'),
(28, 16, 4, 5, '3x1 ', '2019-07-27 23:19:04'),
(29, 21, 1, 5, '3x1 ', '2019-07-29 15:01:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(100) NOT NULL,
  `username_user` varchar(45) NOT NULL,
  `password_user` varchar(45) NOT NULL,
  `level` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_user`, `nama_user`, `username_user`, `password_user`, `level`) VALUES
(1, 'Anggia Ardhanti', 'anggia', '202cb962ac59075b964b07152d234b70', '1'),
(2, 'Andreas Ardani', 'dani', '202cb962ac59075b964b07152d234b70', '1'),
(4, 'Asih Warianti', 'asih', '202cb962ac59075b964b07152d234b70', '2'),
(5, 'Martina', 'tina', '202cb962ac59075b964b07152d234b70', '4'),
(8, 'Thomas Arga', 'arga', '202cb962ac59075b964b07152d234b70', '3');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `biaya_lain`
--
ALTER TABLE `biaya_lain`
  ADD PRIMARY KEY (`id_biaya_lain`);

--
-- Indeks untuk tabel `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`id_dokter`);

--
-- Indeks untuk tabel `icd_diagnosa`
--
ALTER TABLE `icd_diagnosa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kunjungan`
--
ALTER TABLE `kunjungan`
  ADD PRIMARY KEY (`id_kunjungan`),
  ADD KEY `fkIdx_102` (`id_dokter`),
  ADD KEY `fkIdx_61` (`id_pasien`);

--
-- Indeks untuk tabel `obat`
--
ALTER TABLE `obat`
  ADD PRIMARY KEY (`id_obat`);

--
-- Indeks untuk tabel `obat_masuk`
--
ALTER TABLE `obat_masuk`
  ADD PRIMARY KEY (`id_obat_masuk`),
  ADD KEY `fkIdx_70` (`id_obat`);

--
-- Indeks untuk tabel `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id_pasien`);

--
-- Indeks untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indeks untuk tabel `periksa`
--
ALTER TABLE `periksa`
  ADD PRIMARY KEY (`id_periksa`);

--
-- Indeks untuk tabel `resep`
--
ALTER TABLE `resep`
  ADD PRIMARY KEY (`id_resep`),
  ADD KEY `fkIdx_82` (`id_obat`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `username_user` (`username_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `biaya_lain`
--
ALTER TABLE `biaya_lain`
  MODIFY `id_biaya_lain` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `dokter`
--
ALTER TABLE `dokter`
  MODIFY `id_dokter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `icd_diagnosa`
--
ALTER TABLE `icd_diagnosa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;

--
-- AUTO_INCREMENT untuk tabel `kunjungan`
--
ALTER TABLE `kunjungan`
  MODIFY `id_kunjungan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `obat`
--
ALTER TABLE `obat`
  MODIFY `id_obat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `obat_masuk`
--
ALTER TABLE `obat_masuk`
  MODIFY `id_obat_masuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `periksa`
--
ALTER TABLE `periksa`
  MODIFY `id_periksa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `resep`
--
ALTER TABLE `resep`
  MODIFY `id_resep` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
