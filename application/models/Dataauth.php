<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataauth extends CI_Model {

	function auth_user($username,$password){
		$sql = "SELECT * FROM users WHERE username_user = ? AND password_user = md5(?) LIMIT 1";
		$parameter = $this->db->query($sql, array($username,$password));
		return $parameter;
	}

	function auth_dokter($username,$password){
		$sql = "SELECT * FROM dokter WHERE username_dokter = ? AND password_dokter = md5(?) LIMIT 1";
		$parameter = $this->db->query($sql, array($username,$password));
		return $parameter;
	}
}
