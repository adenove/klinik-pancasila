<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Datadiagnosa extends CI_Model{

	public function __construct()
    {
        $this->load->database();
    }

    public function get(){
		return $this->db->get('icd_diagnosa');
    }

    public function get_where($where){
        $this->db->where($where);
		return $this->db->get('icd_diagnosa');
	}

	public function add($data) {
		return $this->db->insert('icd_diagnosa',$data);
    }

    public function update($where,$data){
		$this->db->where($where);
		$this->db->update('icd_diagnosa',$data);
	}

    function delete($where){
		$this->db->where($where);
		$this->db->delete('icd_diagnosa');
	}

}
?>
