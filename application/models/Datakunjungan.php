<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Datakunjungan extends CI_Model{

	public function __construct()
        {
            $this->load->database();
        }

	public function daftar($x,$y){
		$this->db->select('*');
		$this->db->from('kunjungan');
		$this->db->join('pasien', 'kunjungan.id_pasien = pasien.id_pasien');
		$this->db->join('dokter', 'kunjungan.id_dokter = dokter.id_dokter');
		$this->db->where($x, $y);
		return $this->db->get();
	}

	public function add($data) {
		return $this->db->insert('kunjungan',$data);
    }

    public function daftar_pasien(){
		$this->db->select('*');
		$this->db->from('pasien');
		return $this->db->get();
	}

	public function daftar_dokter(){
		return $this->db->get('dokter');
	}

	public function edit($x,$y){
		$this->db->where($x,$y);
		return $this->db->get("kunjungan");
	}

	public function update($where,$data){
		$this->db->where($where);
		$this->db->update('kunjungan',$data);
	}

    function delete($where){
		$this->db->where($where);
		$this->db->delete('kunjungan');
	}
}
