<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Datakecamatan extends CI_Model{

	public function __construct()
        {
            $this->load->database();
        }

	public function daftar(){
		$this->db->select('*');
		$this->db->from('kecamatan');
		return $this->db->get();
	}

	public function add($data) {
		return $this->db->insert('kecamatan',$data);
    }

    public function daftar_kabupaten(){
		return $this->db->get('kabupaten');
	}

	public function update($where,$data){
		$this->db->where($where);
		$this->db->update('kecamatan',$data);
	}

    function delete($where){
		$this->db->where($where);
		$this->db->delete('kecamatan');
	}
}
?>
