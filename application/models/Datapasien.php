<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Datapasien extends CI_Model{

	public function __construct()
        {
            $this->load->database();
        }

	public function daftar(){
		$this->db->select('*');
		$this->db->from('pasien');
		// $this->db->join('kabupaten', 'pasien.id_kabupaten = kabupaten.id_kabupaten');
		// $this->db->join('kecamatan', 'pasien.id_kecamatan = kecamatan.id_kecamatan');
		return $this->db->get();
	}

	public function add($data) {
		return $this->db->insert('pasien',$data);
    }

    public function daftar_kabupaten(){
		return $this->db->get('kabupaten');
	}

	public function daftar_kecamatan(){
		return $this->db->get('kecamatan');
	}
	public function get_kecamatan($x, $y){
		$this->db->select('*');
		$this->db->from('kecamatan');
		// $this->db->where($x, $y);
		return $this->db->get();
	}

	public function id_pasien(){
		$query=$this->db->query('select max(id_pasien) as maxid from pasien');
		return $result=$query->row()->maxid;
	}

	public function nomor_rekam_medis(){
		$query=$this->db->query('select max(nomor_rekam_medis) as maxid from pasien');
		return $result=$query->row()->maxid;
	}

	public function edit($x,$y){
		$this->db->where($x,$y);
		return $this->db->get("pasien");
	}

	public function update($where,$data){
		$this->db->where($where);
		$this->db->update('pasien',$data);
	}

    function delete($where){
		$this->db->where($where);
		$this->db->delete('pasien');
	}

	public function get_pasien($id){
		$this->db->select('*');
		$this->db->from('pasien');
		$this->db->where('id_pasien',$id);
		return $this->db->get()->result();
	}
}
