<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Datalaporan extends CI_Model{

	public function __construct()
        {
            $this->load->database();
        }

	public function daftar_obat(){
		return $this->db->get('obat');
	}

	public function daftar_kunjungan($x,$y){
		$sql = $this->db->query("SELECT * FROM kunjungan, pasien, dokter WHERE kunjungan.id_pasien=pasien.id_pasien AND kunjungan.id_dokter=dokter.id_dokter AND tanggal_kunjungan BETWEEN '$x' AND '$y'");
		return $sql;
	}

	public function daftar_pembayaran($x,$y){
		$sql = $this->db->query("SELECT * FROM pembayaran, kunjungan, pasien, dokter WHERE pembayaran.id_kunjungan=kunjungan.id_kunjungan AND kunjungan.id_pasien=pasien.id_pasien AND kunjungan.id_dokter=dokter.id_dokter AND tanggal_pembayaran BETWEEN '$x' AND '$y'");
		return $sql;
	}

	public function daftar_periksa($x,$y){
		$this->db->select('*');
		$this->db->from('periksa');
		$this->db->join('kunjungan', 'periksa.id_kunjungan = kunjungan.id_kunjungan');
		$this->db->join('pasien', 'kunjungan.id_pasien = pasien.id_pasien');
		$this->db->join('dokter', 'kunjungan.id_dokter = dokter.id_dokter');
		$this->db->group_start();
		$this->db->where('kunjungan_status', 'resep');
		$this->db->or_where('kunjungan_status', 'lunas');
		$this->db->group_end();
		$this->db->where('kunjungan.tanggal_kunjungan BETWEEN "'. $x. '" and "'. $y.'"');
		$this->db->order_by('kunjungan.tanggal_kunjungan','DESC');
		return $this->db->get();
	}

	public function daftar_riwayat($id){
		$this->db->select('*');
		$this->db->from('periksa');
		$this->db->join('kunjungan', 'periksa.id_kunjungan = kunjungan.id_kunjungan');
		$this->db->join('pasien', 'kunjungan.id_pasien = pasien.id_pasien');
		$this->db->join('dokter', 'kunjungan.id_dokter = dokter.id_dokter');
		$this->db->where('kunjungan.id_kunjungan', $id);
		return $this->db->get();
	}
}
?>
