<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
 
class Datapembayaran extends CI_Model{
	
	public function __construct()
        {
            $this->load->database();
        }
		
	public function daftar($x, $y){
		$this->db->select('*');    
		$this->db->from('resep');
		$this->db->join('kunjungan', 'resep.id_kunjungan = kunjungan.id_kunjungan');
		$this->db->join('obat', 'resep.id_obat = obat.id_obat');
		$this->db->where($x, $y);
		return $this->db->get();
	}

	public function daftar_biaya_lain($x, $y){
		$this->db->select('*');    
		$this->db->from('biaya_lain');
		$this->db->join('kunjungan', 'biaya_lain.id_kunjungan = kunjungan.id_kunjungan');
		$this->db->where($x, $y);
		return $this->db->get();
	}
	
	public function addBiayaLain($data) {
		return $this->db->insert('biaya_lain',$data);
    }

    public function add($data) {
		return $this->db->insert('pembayaran',$data);
    }

	public function update_status($id,$dataUpdate){
		$this->db->where('id_kunjungan',$id);
		$this->db->update('kunjungan',$dataUpdate);
	}
}
