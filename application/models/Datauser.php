<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
 
class Datauser extends CI_Model{
	
	public function __construct()
        {
            $this->load->database();
        }
		
	public function daftar(){
		return $this->db->get('users');
	}
	
	public function add($data) {
		return $this->db->insert('users',$data); 
    }

    public function edit($x,$y){
			$this->db->where($x,$y);
			return $this->db->get("users");
	}
	
    public function update($where,$data){
		$this->db->where($where);
		$this->db->update('users',$data);
	}

    function delete($where){
		$this->db->where($where);
		$this->db->delete('users');
	}
}
?>
