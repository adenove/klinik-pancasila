<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
 
class Datakabupaten extends CI_Model{
	
	public function __construct()
        {
            $this->load->database();
        }
		
	public function daftar(){
		return $this->db->get('kabupaten');
	}
	
	public function add($data) {
		return $this->db->insert('kabupaten',$data); 
    }

    public function update($where,$data){
		$this->db->where($where);
		$this->db->update('kabupaten',$data);
	}

    function delete($where){
		$this->db->where($where);
		$this->db->delete('kabupaten');
	}
}
?>
