<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Databeranda extends CI_Model{

	public function __construct()
        {
            $this->load->database();
        }

	function jumlah_dokter(){
		return $this->db->query ("SELECT * FROM dokter");

	}

	function jumlah_pasien($x){
		// return $this->db->query ("SELECT * FROM pasien");
		$this->db->select('*');
		$this->db->from('kunjungan');
		$this->db->where('DATE(kunjungan.tanggal_kunjungan)',$x);
		return $this->db->get();

	}

	function jumlah_kunjungan(){
		return $this->db->query ("SELECT * FROM kunjungan");

	}

	function jumlah_petugas(){
		return $this->db->query ("SELECT * FROM users");

	}
}
?>
