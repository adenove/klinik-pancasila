<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datarekap extends CI_Model {
	function kunjungan_bulanan($x){
		// $sql = "SELECT *,COUNT(MONTH(tanggal_kunjungan)) AS jumlah, MONTH(tanggal_kunjungan) AS bulan FROM kunjungan WHERE YEAR(tanggal_kunjungan) = ? GROUP BY MONTH(tanggal_kunjungan)";
		// $parameter = $this->db->query($sql, array($x));
		$this->db->select('*');
		$this->db->from('kunjungan');
		$this->db->where('YEAR(tanggal_kunjungan)',$x);
		$parameter = $this->db->get();
		return $parameter;
	}
}
