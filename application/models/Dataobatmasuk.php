<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
 
class Dataobatmasuk extends CI_Model{
	
	public function __construct()
        {
            $this->load->database();
        }
		
	public function daftar(){
		$this->db->select('*');    
		$this->db->from('obat_masuk');
		$this->db->join('obat', 'obat_masuk.id_obat = obat.id_obat');
		return $this->db->get();
	}
	
	public function add($data) {
		return $this->db->insert('obat_masuk',$data);
    }

    public function tambah_stok($id_obat,$dataUpdate){
		$this->db->where('id_obat',$id_obat);
		$this->db->update('obat',$dataUpdate);
	}

    public function stok_obat($id_obat){
    	$this->db->select('stok_obat');
    	$this->db->where('id_obat',$id_obat);
		return $this->db->get('obat');
	}

	public function filter($x,$y){
    	$this->db->where($x,$y);
		return $this->db->get('obat_masuk');
	}

	public function daftar_obat(){
		return $this->db->get('obat');
	}

	public function update($where,$data){
		$this->db->where($where);
		$this->db->update('obat_masuk',$data);
	}

    function delete($where){
		$this->db->where($where);
		$this->db->delete('obat_masuk');
	}
}
