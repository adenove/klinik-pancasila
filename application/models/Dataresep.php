<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Dataresep extends CI_Model{

	public function __construct()
        {
            $this->load->database();
        }

	public function daftar($x, $y){
		$this->db->select('*');
		$this->db->from('resep');
		$this->db->join('kunjungan', 'resep.id_kunjungan = kunjungan.id_kunjungan');
		$this->db->join('obat', 'resep.id_obat = obat.id_obat');
		$this->db->where($x, $y);
		return $this->db->get();
	}

	public function daftar_periksa(){
		$this->db->select('*');
		$this->db->from('periksa');
		$this->db->join('kunjungan', 'periksa.id_kunjungan = kunjungan.id_kunjungan');
		$this->db->join('pasien', 'kunjungan.id_pasien = pasien.id_pasien');
		$this->db->join('dokter', 'kunjungan.id_dokter = dokter.id_dokter');
		$this->db->where('kunjungan_status', 'resep');
		$this->db->or_where('kunjungan_status', 'lunas');
		return $this->db->get();
	}

	public function riwayat($x,$y){
		$this->db->select('*');
		$this->db->from('resep');
		$this->db->join('kunjungan', 'resep.id_kunjungan = kunjungan.id_kunjungan');
		$this->db->join('obat', 'resep.id_obat = obat.id_obat');
		$this->db->join('pasien','kunjungan.id_pasien = pasien.id_pasien');
		$this->db->where('resep.tanggal BETWEEN "'. $x. '" and "'. $y.'"');
		return $this->db->get();
	}

	//resep
	public function add($data) {
		return $this->db->insert('resep',$data);
    }

    //periksa
    public function add_periksa($data) {
		return $this->db->insert('periksa',$data);
    }

    public function kurangi_stok($id_obat,$dataUpdate){
		$this->db->where('id_obat',$id_obat);
		$this->db->update('obat',$dataUpdate);
	}

    public function stok_obat($id_obat){
    	$this->db->select('stok_obat');
    	$this->db->where('id_obat',$id_obat);
		return $this->db->get('obat');
	}

	public function update_status($id,$dataUpdate){
		$this->db->where('id_kunjungan',$id);
		$this->db->update('kunjungan',$dataUpdate);
	}

	public function daftar_obat(){
		return $this->db->get('obat');
	}

	public function obat_by_id($id){
		$this->db->select('*');
		$this->db->from('resep');
		$this->db->join('obat','resep.id_obat = obat.id_obat');
		$this->db->where('id_resep',$id);
		$resep = $this->db->get()->result();

		$restore = $resep[0]->jumlah_obat_keluar + $resep[0]->stok_obat;

		$data = array(
			'stok_obat' => $restore
		);

		$where = array(
			'id_obat' => $resep[0]->id_obat
		);

		$this->dataobat->update($where,$data);

		echo 'obat restore';

	}

	public function delete($id) {
		$this->db->where('id_resep',$id);
		$this->db->delete('resep');
	}

	public function get_edit(){
		$id = $this->input->post('id');
		$this->db->select('*');
		$this->db->from('resep');
		$this->db->where('id_resep',$id);
		$result = $this->db->get()->result();
		echo json_encode($result);
	}


	public function update($data,$id_resep){

		$where = array(
			'id_resep' => $id_resep
		);

	}

	public function edit_resep(){
		$id = $this->input->post('id_resep');
		$this->db->select('*');
		$this->db->from('resep');
		$this->db->join('obat','resep.id_obat = obat.id_obat');
		$this->db->where('id_resep',$id);
		$result = $this->db->get()->result();
		$stok_keluar = $this->input->post('jumlah_obat_keluar');
		$restore = ($result[0]->jumlah_obat_keluar + $result[0]->stok_obat)-$stok_keluar;

		$xid = $this->input->post('id_kunjungan');
		$id_kunjungan = de($xid);
		$id_obat = $this->input->post('id_obat');
		$dataUpdate = array(
			'id_kunjungan' => $id_kunjungan,
			'id_obat' => $id_obat,
			'jumlah_obat_keluar' => $stok_keluar,
			'keterangan' => $this->input->post('keterangan')
		);

		// $this->update($dataUpdate,$id);
		echo 'stock awal'.($result[0]->jumlah_obat_keluar + $result[0]->stok_obat);
		echo 'stok obat'.$restore;
		echo '/obat keluar'.$stok_keluar;
	}

}
