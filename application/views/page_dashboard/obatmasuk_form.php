<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Daftar <?php echo $title ?></h4>
				</div>
				<div class="card-body">
					<form id="" method="post" action="<?php echo base_url('obatmasuk/create') ?>">
						<div class="card-body">
							<?php input_option("Nama Obat","select2"," - ",$arrayIdObat,$arrayNamaObat,"id_obat","","required","") ?>
							<?php input_text("Harga Beli","text","","harga_beli","","","required","")?>
							<?php input_text("Jumlah Obat","text","","jumlah_obat_masuk","","","required","")?>
						</div>
						<div class="card-action">
							<?php input_button() ?>
						</div>						
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
