<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Input <?php echo $title ?></h4>
				</div>
				<div class="card-body">
					<form id="" method="post" action="<?php echo base_url($action) ?>">
						<div class="card-body">
							<div class="card-body">
							<div hidden><?php input_text("ID","text","","id",$xid_obat,"","","")?></div>
							<?php input_text("Nama Obat","text","","nama",$nama_obat,"","required","")?>
							<?php input_text("Harga Jual","text","","harga",$harga_jual,"","required","")?>
						</div>
						<div class="card-action">
							<?php input_button() ?>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
