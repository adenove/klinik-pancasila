<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Daftar <?php echo $title ?></h4>
				</div>
				<div class="card-body">
					<form id="" method="post" action="<?php echo base_url($action) ?>">
						<div class="card-body">
							<div hidden><?php input_text("ID","text","","id",$xid_dokter,"","","")?></div>
							<?php input_text("Nama Dokter","text","","nama",$nama_dokter,"","required","")?>
							<?php input_text("Username","text","","username",$username_dokter,"","required","")?>
							<?php input_text("Password","password","","password","","","required","")?>
						</div>
						<div class="card-action">
							<?php input_button() ?>
						</div>						
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
