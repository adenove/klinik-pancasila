<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Daftar <?php echo $title ?></h4>
				</div>
				<div class="card-body">
					<form id="" method="post" action="<?php echo base_url($action) ?>">
						<div class="card-body">
							<div class="card-body">
							<div hidden><?php input_text("ID","text","","id",$xid_user,"","","")?></div>
							<?php input_text("Nama Petugas","text","","nama",$nama_user,"","required","")?>
							<?php input_text("Username","text","","username",$username_user,"","required","")?>
							<?php input_text("Password","password","","password","","","required","")?>
							<?php input_option("Level","select2"," - ",[1,2,3,4],[text_level(1),text_level(2),text_level(3),text_level(4)],"level",$level,"required","") ?>
						</div>
						<div class="card-action">
							<?php input_button() ?>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
