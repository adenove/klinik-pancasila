<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Daftar <?php echo $title ?></h4>
				</div>
				<div class="card-body">
					<!-- <?php echo button_add($title,'href="'.base_url('kunjungan/input').'"') ?><br><br> -->
					<div class="table-responsive">
						<table id="basic-datatables" class="display table table-striped table-hover" >
							<thead>
								<tr>
									<th>No</th>
									<th>Pasien</th>
									<th>Tgl</th>
									<th>Jenis</th>
									<th>Keluhan</th>
									<th>Dokter</th>
									<th>Hapus</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no=1;
									foreach ($data as $d):
								?>
								<tr>
									<td><?php echo $no++ ?></td>
									<td><?php echo $d->nama_pasien ?></td>
									<td><?php echo tanggal($d->tanggal_kunjungan) ?></td>
									<td><?php echo text_jeniskunjungan($d->jenis_kunjungan) ?></td>
									<td><?php echo $d->keluhan ?></td>
									<td><?php echo $d->nama_dokter ?></td>
									<!-- <td width="20%" align="center"><?php echo button_ubah(base_url("pasien/edit/".en($d->id_pasien)))?></td> -->
									<td width="20%" align="center"><?php echo button_delete("kunjungan/delete/".en($d->id_kunjungan))?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
