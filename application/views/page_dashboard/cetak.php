<style>
.cards {
    width:418px;
    height:232px;
    border:1px solid black;
    margin-right:20px;
    border-radius:0px;
    position:relative;
}

.back {
    display:flex;
    justify-content:center;
    align-items:center;
}

.cards .group {
    display:flex;
    flex-direction:column;
    justify-content:center;
    align-items:center;
}

.cards .group h4 {
    margin-top:5px;
    font-family: 'Lato', sans-serif;
    font-size:17px;
}

.heades {
    padding:2px;
    display:flex;
    align-items:center;
    padding-bottom:10px;
    border-bottom:1px solid black;
}


.heades h4 {
    margin:0px;
    margin-left:10px;
    font-size:15px;
    margin-bottom:5px;
}


.heades h5 {
    font-family: 'Lato', sans-serif;
    margin:0px;
    margin-left:10px;
    font-size:13px;
}

.groups h3 {
    font-size:12px;
    margin:0px;
}

.isflex {
    display:flex;
    align-items:center;
}

table, th, td {
  /* border: 1px solid black; */
  border-collapse: collapse;
  font-size:11.9px;
}

</style>
<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Daftar <?php echo $title ?></h4>
				</div>
				<div class="card-body">
                <br>
                <br>
                    <div class="row justify-content-center" id="section-to-print">
                        <div class="cards front">
                            <div class="heades">
                                <img width="50px" height="50px" src="<?php echo base_url('assets/img/logo.png');?>" alt="logo">
                                <div class="gr text-center">
                                    <h4> KLINIK PANCASILA BATURETNO </h4>
                                    <h5> JANGLOT-BATURETNO Telp.(0273) 461004 </h5>
                                </div>
                            </div>
                            <div style="padding:5px 10px;" >
                                <table style="width:100%">
                                    <tr>
                                        <th>NO RM</th>
                                        <td>: <?php echo $kartu[0]->nomor_rekam_medis;?></td>
                                    </tr>
                                    <tr>
                                        <th>Nama</th>
                                        <td>: <?php echo $kartu[0]->nama_pasien;?></td>
                                    </tr>
                                    <tr>
                                        <th>Umur / Tanggal lahir</th>
                                        <td>: <?php echo $kartu[0]->usia;?> / <?php echo $kartu[0]->tanggal_lahir;?></td>
                                    </tr>
                                    <tr>
                                        <th>Alamat</th>
                                        <td>: <?php echo $kartu[0]->alamat;?>,<?php echo $kartu[0]->kecamatan;?>,<?php echo $kartu[0]->kabupaten;?>,<?php echo $kartu[0]->provinsi;?></td>
                                    </tr>
                                </table>
                            </div>
                            <em style="font-size:11px;margin-top:15px;margin-left:10px;color:#64ddf6;position:absolute;bottom:5px;" > <b> Kartu Harap Dibawa Setiap Kali berobat/kunjungan </b> </em>
                        </div>
                    </div>
                    <br>
                    <br>
                    <hr>
                    <button class="btn btn-primary" id="print_k">Cetak Kartu</button>
				</div>
			</div>
		</div>
	</div>
</div>
