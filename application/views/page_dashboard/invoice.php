<div class="page-inner">
	<div class="row">
		<div id="print_area" class="col-md-12">
			<div class="card card-invoice">
				<div class="card-header">
					<div class="invoice-header">
						<h3 class="invoice-title">
							Pembayaran
						</h3>
						<div class="logo">
							<img src="<?php echo base_url() ?>assets/img/logo_text.png" alt="">
						</div>
					</div>
					<div class="invoice-desc">
						Jl. Wonogiri-Pacitan, Jang Lot, Baturetno,<br/>
						Kabupaten Wonogiri, Jawa Tengah 57673
					</div>
				</div>
				<div class="card-body">
					<div class="separator-solid"></div>

					<?php
						foreach ($dataKunjungan as $d):
					?>
					<div class="row">
						<div class="col-md-4 info-invoice">
							<h5 class="sub">No. Pembayaran</h5>
							<p>BY000<?php echo $d->id_kunjungan ?></p>
						</div>
						<div class="col-md-4 info-invoice">
							<h5 class="sub">Tgl Berobat</h5>
							<p><?php echo tanggal($d->tanggal_kunjungan) ?></p>
						</div>
						<div class="col-md-4 info-invoice">
							<h5 class="sub">Resep untuk</h5>
							<p>
								<?php echo $d->nama_pasien ?><br/>
								<?php echo $d->alamat ?><br/>
								<?php echo $d->kelurahan.', '.$d->kecamatan.', '.$d->kabupaten.', '.$d->provinsi ?>
							</p>
						</div>
					</div>
					<?php endforeach; ?>

					<div class="row">
						<div class="col-md-12">
							<div class="invoice-detail">
								<div class="invoice-top">
									<h3 class="title"><strong>Biaya Obat</strong></h3>
								</div>
								<div class="invoice-item">
									<div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr>
													<td><strong>No</strong></td>
													<td class="text-left"><strong>Obat</strong></td>
													<td class="text-left"><strong>Keterangan</strong></td>
													<td class="text-left"><strong>Harga Satuan</strong></td>
													<td class="text-center"><strong>Qty</strong></td>
													<td class="text-right"><strong>Totals</strong></td>
												</tr>
											</thead>
											<tbody>
											<?php
												$no=1;
												$jumlah_total = 0;
												foreach ($dataResep as $d):
												$total = $d->harga_jual * $d->jumlah_obat_keluar;
												$jumlah_total = $total + $jumlah_total;
											?>
												<tr>
													<td><?php echo $no++ ?></td>
													<td class="text-left"><?php echo $d->nama_obat ?></td>
													<td class="text-justify"><?php echo $d->keterangan ?></td>
													<td class="text-right"><?php echo rupiah($d->harga_jual) ?></td>
													<td class="text-center"><?php echo $d->jumlah_obat_keluar ?></td>
													<td class="text-right"><?php echo rupiah($total) ?></td>
												</tr>
											<?php endforeach; ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="invoice-detail">
								<div class="invoice-top">
									<h3 class="title"><strong>Biaya Lainnya</strong></h3>
								</div>
								<div class="invoice-item">
									<div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr>
													<td><strong>No</strong></td>
													<td class="text-left"><strong>Layanan</strong></td>
													<td class="text-left"><strong>Biaya</strong></td>
													<td class="text-left"><strong>Jumlah</strong></td>
													<td class="text-right"><strong>Total</strong></td>
												</tr>
											</thead>
											<tbody>
											<?php
												$no=1;
												$jumlah_total2 = 0;
												foreach ($dataBiayaLain as $d):
												$total2 = $d->biaya_layanan * $d->jumlah_layanan;
												$jumlah_total2 = $total2 + $jumlah_total2;
											?>
												<tr>
													<td><?php echo $no++ ?></td>
													<td class="text-left"><?php echo $d->jenis_layanan ?></td>
													<td class="text-justify"><?php echo rupiah($d->biaya_layanan) ?></td>
													<td class="text-justify"><?php echo $d->jumlah_layanan ?></td>
													<td class="text-right"><?php echo rupiah($total2) ?></td>
												</tr>
											<?php endforeach; ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="separator-solid  mb-3"></div>
							<?php echo button_add('Biaya Lainnya','data-toggle="modal" data-target="#input"') ?>
						</div>
					</div>
				</div>
				<div class="card-footer">
					<div class="row">
						<div class="col-sm-7 col-md-5 mb-3 mb-md-0 transfer-to">
						</div>
						<div class="col-sm-5 col-md-7 transfer-total">
							<h5 class="sub">Jumlah Total</h5>
							<div class="price"><?php echo rupiah($jumlah_total+$jumlah_total2) ?></div>
							<span></span>
						</div>
					</div>
					<div class="separator-solid"></div>
					<h6 class="text-uppercase mt-4 mb-3 fw-bold">
						Perhatian
					</h6>
					<p class="text-muted mb-0">
						Terima kasih telah mengurus administrasi pembayaran, semoga lekas sembuh.
					</p>
				</div>
				<div class="card-header">
					<div class="invoice-desc">
						<?php
						if($d->kunjungan_status !== 'resep')
							{ echo '';}
						else { ?>
							<form id="" method="post" action="<?php echo base_url('pembayaran/create') ?>">
								<div class="card-body">
									<div hidden><?php input_text("Jumlah","text","","id_kunjungan",$id_kunjungan,"","required","")?></div>
									<div hidden><?php input_text("Jumlah Transaksi","text","","total_pembayaran",$jumlah_total+$jumlah_total2,"","required","")?></div>
								</div>
								<div class="">
									<?php button_ok("Bayar","") ?>
								</div>
							</form>
							<?php ;}?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="editor"></div>

<!-- Modal Input-->
<div class="modal fade" id="input" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Input Biaya Lainnya</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="" method="post" action="<?php echo base_url('pembayaran/create_biaya_lain') ?>">
					<div class="card-body">
						<div hidden><?php input_text("Jumlah","text","","id_kunjungan",$id_kunjungan,"","required","")?></div>
						<?php input_text("Layanan","text","","jenis_layanan","","","required","")?>
						<?php input_text("Biaya","text","","biaya_layanan","","","required","")?>
						<?php input_text("Jumlah","text","","jumlah_layanan","","","required","")?>
					</div>
					<div class="card-action">
						<?php input_button() ?>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /Modal Input-->


