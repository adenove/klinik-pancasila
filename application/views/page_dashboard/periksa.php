<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Daftar <?php echo $title ?></h4>
				</div>
				<div class="card-body">
					<div class="table-responsive">
					<?php if ($this->session->userdata('level') == "1")
						{ ?>
						<table id="basic-datatables" class="display table table-striped table-hover" >
							<thead>
								<tr>
									<th>No</th>
									<th>No RM</th>
									<th>Pasien</th>
									<th>Tgl</th>
									<th>Jenis</th>
									<th>Keluhan</th>
									<th>ICD</th>
									<th>Diagnosa</th>
									<th>Tindakan</th>
									<th>Dokter</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no=1;
									foreach ($data as $d):
								?>
								<tr>
									<td><?php echo $no++ ?></td>
									<td><?php echo $d->nomor_rekam_medis ?></td>
									<td><?php echo $d->nama_pasien ?></td>
									<td><?php echo tanggal($d->tanggal_kunjungan) ?></td>
									<td><?php echo text_jeniskunjungan($d->jenis_kunjungan) ?></td>
									<td><?php echo $d->keluhan ?></td>
									<td><?php echo $d->icd ?></td>
									<td><?php echo $d->diagnosa ?></td>
									<td><?php echo $d->tindakan ?></td>
									<td><?php echo $d->nama_dokter ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					<?php ;}
					else { ?>

						<table id="basic-datatables" class="display table table-striped table-hover" >
							<thead>
								<tr>
									<th>No</th>
									<th>Pasien</th>
									<th>Tgl</th>
									<th>Jenis</th>
									<th>Keluhan</th>
									<th>ICD</th>
									<th>Diagnosa</th>
									<th>Tindakan</th>
									<th>Dokter</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no=1;
									foreach ($data as $d):
										if ($d->id_dokter !== $this->session->userdata('id'))
											{ $show = '';}
										else {
								?>
								<tr>
									<td><?php echo $no++ ?></td>
									<td><?php echo $d->nama_pasien ?></td>
									<td><?php echo tanggal($d->tanggal_kunjungan) ?></td>
									<td><?php echo text_jeniskunjungan($d->jenis_kunjungan) ?></td>
									<td><?php echo $d->keluhan ?></td>
									<td><?php echo $d->icd ?></td>
									<td><?php echo $d->diagnosa ?></td>
									<td><?php echo $d->tindakan ?></td>
									<td><?php echo $d->nama_dokter ?></td>
								</tr>
								<?php } endforeach; ?>
							</tbody>
						</table>
					<?php ;} ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>