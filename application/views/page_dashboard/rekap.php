<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Daftar <?php echo $title ?></h4>
				</div>
				<div class="card-body">
					<div class="chart-container">
						<canvas id="barChart"></canvas>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
		var barChart = document.getElementById('barChart').getContext('2d');

		var myBarChart = new Chart(barChart, {
			type: 'bar',
			data: {
				labels: <?php echo json_encode($arrayBulan) ?>,
				datasets : [{
					label: '<?php echo $labels ?>',
					backgroundColor: 'rgb(23, 125, 255)',
					borderColor: 'rgb(23, 125, 255)',
					data: <?php echo json_encode($arrayJumlah) ?>,
				}],
			},
			options: {
				responsive: true,
				maintainAspectRatio: false,
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero:true,
							userCallback: function(label, index, labels) { //tanpa koma
				             if (Math.floor(label) === label) {
				               return label;
				             }

				           },
						}
					}]
				},
			}
		});
	</script>
