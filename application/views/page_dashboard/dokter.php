<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Daftar <?php echo $title ?></h4>
				</div>
				<div class="card-body">
					<?php echo button_add($title,'href="'.base_url('dokter/input').'"') ?><br><br>
					<div class="table-responsive">
						<table id="basic-datatables" class="display table table-striped table-hover" >
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Usename</th>
									<th>Pilihan</th>
								</tr>
							</thead>
							<tbody>
								<?php 
	  								$no=1;
	  								foreach ($data as $d):
  								?>
								<tr>
									<td width="10%"><?php echo $no++ ?></td>
									<td><?php echo $d->nama_dokter ?></td>
									<td><?php echo $d->username_dokter ?></td>
									<td width="20%" align="center"><?php echo button_ubah(base_url("dokter/edit/".en($d->id_dokter))).' '.button_delete("dokter/delete/".en($d->id_dokter))?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
