<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title"><?php echo $title ?></h4>
				</div>
				<div class="card-body">
					<form id="" method="post" action="<?php echo base_url($action) ?>">
						<div class="card-body">
							<?php input_date("Mulai Tanggal","text","date","awal","","required","") ?>
							<?php input_date("Sampai Tanggal","text","date2","akhir","","required","") ?>
							<?php button_ok("Ok","") ?>
						</div>
						<div class="card-action"></div>
					</form>
					<br>
					<div class="table-responsive">
						<table id="basic-datatables" class="display table table-striped table-hover" >
							<thead>
								<tr>
									<th>No</th>
									<th>Pasien</th>
									<th>Tgl</th>
									<th>Jenis</th>
									<th>Keluhan</th>
									<th>Dokter</th>
									<th>Pilihan</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no=1;
									foreach ($data as $d):
								?>
								<tr>
									<td><?php echo $no++ ?></td>
									<td><?php echo $d->nama_pasien ?></td>
									<td><?php echo tanggal($d->tanggal_kunjungan) ?></td>
									<td><?php echo text_jeniskunjungan($d->jenis_kunjungan) ?></td>
									<td><?php echo $d->keluhan ?></td>
									<td><?php echo $d->nama_dokter ?></td>
									<td width="20%" align="center">
										<a style="margin-top:5px;margin-bottom:5px; width:100px;" class="btn btn-success" href="<?php echo base_url() ?>pembayaran/invoice/<?php echo en($d->id_kunjungan) ?>"></i> Detail</a>
										<a style="margin-bottom:5px; width:100px;" class="btn btn-primary" href="<?php echo base_url() ?>pembayaran/print/<?php echo en($d->id_kunjungan) ?>">Print</a>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
