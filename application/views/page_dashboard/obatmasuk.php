<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Daftar <?php echo $title ?></h4>
				</div>
				<div class="card-body">
					<?php echo button_add($title,'href="'.base_url('obatmasuk/input').'"') ?><br><br>
					<div class="table-responsive">
						<table id="basic-datatables" class="display table table-striped table-hover" >
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Obat</th>
									<th>Harga Beli</th>
									<th>Jumlah</th>
									<th>Tgl Masuk</th>
									<th>Pilihan</th>
								</tr>
							</thead>
							<tbody>
								<?php 
	  								$no=1;
	  								foreach ($data as $d):
  								?>
								<tr>
									<td width="10%"><?php echo $no++ ?></td>
									<td><?php echo $d->nama_obat ?></td>
									<td><?php echo rupiah($d->harga_beli) ?></td>
									<td><?php echo $d->jumlah_obat_masuk ?></td>
									<td><?php echo tanggal($d->tanggal_obat_masuk) ?></td>
									<td width="20%" align="center"><?php echo button_delete("obatmasuk/delete/".en($d->id_obat_masuk))?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
