<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title"><?php echo $title ?></h4>
				</div>
				<div class="card-body">
					<br>
					<div class="table-responsive">
						<table id="basic-datatables" class="display table table-striped table-hover" >
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Harga Jual</th>
									<th>Stok</th>
								</tr>
							</thead>
							<tbody>
								<?php 
	  								$no=1;
	  								foreach ($data as $d):
  								?>
								<tr>
									<td width="10%"><?php echo $no++ ?></td>
									<td><?php echo $d->nama_obat ?></td>
									<td><?php echo rupiah($d->harga_jual) ?></td>
									<td><?php echo $d->stok_obat ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
