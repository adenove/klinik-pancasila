<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Daftar <?php echo $title ?></h4>
				</div>
				<div class="card-body">
					<?php echo button_add($title,'data-toggle="modal" data-target="#input"') ?><br><br>
					<div class="table-responsive">
						<table id="basic-datatables" class="display table table-striped table-hover" >
							<thead>
								<tr>
									<th>No</th>
									<th>ICD</th>
									<th>Diagnosa</th>
									<th>Pilihan</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no=1;
									foreach ($data as $d):
								?>
								<tr>
									<td><?php echo $no++ ?></td>
									<td><?php echo $d->icd ?></td>
									<td><?php echo $d->diagnosa ?></td>
									<td width="20%" align="center"><?php echo button_icd('editicd',$d->id).' '.button_delete("diagnosa/delete/".en($d->id))?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						<!-- Modal Input-->
						<div class="modal fade" id="input" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Input <?php echo $title ?></h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<form id="" method="post" action="<?php echo base_url('diagnosa/create') ?>">
											<div class="card-body">
                                                <?php input_text("ICD","text","","icd","","","required","")?>
												<?php input_text("Diagnosa","text","","diagnosa","","","required","")?>
											</div>
											<div class="card-action">
												<?php input_button() ?>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						<!-- /Modal Input-->
                        <!-- Modal edit-->
						<div class="modal fade" id="editdiagnosa" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Edit <?php echo $title ?></h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<form id="" method="post" action="<?php echo base_url('diagnosa/update') ?>">
											<div class="card-body">
                                                <div hidden ><?php input_text("id","text","id_icd","id","","","required","")?></div>
                                                <?php input_text("ICD","text","icd","icd","","","required","")?>
												<?php input_text("Diagnosa","text","diagnosa","diagnosa","","","required","")?>
											</div>
											<div class="card-action">
												<?php input_button() ?>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						<!-- /Modal Input-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
