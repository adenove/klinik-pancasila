<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Daftar <?php echo $title ?></h4>
				</div>
				<div class="card-body">
					<?php echo button_add($title,'href="'.base_url('user/input').'"') ?><br><br>
					<div class="table-responsive">
						<table id="basic-datatables" class="display table table-striped table-hover" >
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Usename</th>
									<th>Level</th>
									<th>Pilihan</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no=1;
									foreach ($data as $d):
								?>
								<tr>
									<td><?php echo $no++ ?></td>
									<td><?php echo $d->nama_user ?></td>
									<td><?php echo $d->username_user ?></td>
									<td><?php echo text_level($d->level) ?></td>
									<td width="20%" align="center"><?php echo button_ubah(base_url("user/edit/".en($d->id_user))).' '.button_delete("user/delete/".en($d->id_user))?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
