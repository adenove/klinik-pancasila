<?php if ($this->session->userdata('level') == '1') { ?>
	<div class="sidebar sidebar-style-2" data-background-color="white">
		<div class="sidebar-wrapper scrollbar scrollbar-inner">
			<div class="sidebar-content">
				<ul class="nav nav-primary">
					<li class="nav-item active">
						<a>
							<i class="fas fa-user"></i>
							<p>Menu <?php echo text_level($this->session->userdata('level'))?></p>
						</a>
					</li>
				</ul>
				<ul class="nav nav-primary">
					<li class="nav-section">
						<span class="sidebar-mini-icon">
							<i class="fa fa-ellipsis-h"></i>
						</span>
						<h4 class="text-section">Pilihan</h4>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('beranda') ?>">
							<i class="fas fa-home"></i>
							<p>Beranda</p>
						</a>
					</li>
					<li class="nav-item">
						<a data-toggle="collapse" href="#manajemen">
							<i class="flaticon-users"></i>
							<p>Manajemen user</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="manajemen">
							<ul class="nav nav-collapse">
								<li>
									<a href="<?php echo base_url('user') ?>">
										<span class="sub-item">User</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url('dokter') ?>">
									<span class="sub-item">Dokter</span>
									</a>
								</li>
							</ul>
						</div>
					</li>
					<li class="nav-item">
						<a data-toggle="collapse" href="#datapasien">
							<i class="flaticon-user-6"></i>
							<p>Data Pasien</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="datapasien">
							<ul class="nav nav-collapse">
								<li>
									<a href="<?php echo base_url('pasien') ?>">
										<span class="sub-item">Pasien</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url('kunjungan') ?>">
										<span class="sub-item" >Kunjungan</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url('rekap') ?>">
										<span class="sub-item">Rekap Kunjungan</span>
									</a>
								</li>
							</ul>
						</div>
					</li>
					<li class="nav-item">
						<a data-toggle="collapse" href="#datapemeriksaan">
							<i class="fas fa-medkit"></i>
							<p>Data Pemeriksaan</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="datapemeriksaan">
							<ul class="nav nav-collapse">
								<li>
									<a href="<?php echo base_url('periksa') ?>">
										<span class="sub-item">Periksa</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url('periksa/daftar_selesai_periksa') ?>">
										<span class="sub-item">Selesai Periksa</span>
									</a>
								</li>
							</ul>
						</div>
					</li>
					<li class="nav-item">
						<a data-toggle="collapse" href="#dataobat">
							<i class="fas fa-capsules"></i>
							<p>Data Obat</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="dataobat">
							<ul class="nav nav-collapse">
								<li>
									<a href="<?php echo base_url('obatmasuk') ?>">
										<span class="sub-item">Obat Masuk</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url('obat') ?>">
										<span class="sub-item">Obat</span>
									</a>
								</li>
							</ul>
						</div>
					</li>
					<li class="nav-item">
						<a data-toggle="collapse" href="#pembayaran">
							<i class="flaticon-credit-card-1"></i>
							<p>Data Pembayaran</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="pembayaran">
							<ul class="nav nav-collapse">
								<li>
									<a href="<?php echo base_url('pembayaran') ?>">
										<span class="sub-item">Pembayaran (Blm Lunas)</span>
									</a>
								</li>
								<li >
									<a href="<?php echo base_url('pembayaran/lunas') ?>">
										<span class="sub-item">Pembayaran Lunas</span>
									</a>
								</li>
							</ul>
						</div>
					</li>

					<li class="nav-item" hidden>
						<a href="<?php echo base_url('kabupaten') ?>">
							<i class="fas fa-map-signs"></i>
							<p>Kabupaten</p>
						</a>
					</li>
					<li class="nav-item" hidden>
						<a href="<?php echo base_url('kecamatan') ?>">
							<i class="fas fa-map-marker-alt"></i>
							<p>Kecamatan</p>
						</a>
					</li>
					<li class="nav-item">
						<a data-toggle="collapse" href="#charts">
							<i class="fas fa-book"></i>
							<p>Laporan</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="charts">
							<ul class="nav nav-collapse">
								<li>
									<a href="<?php echo base_url('laporan/obat') ?>">
										<span class="sub-item">Laporan Stok Obat</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url('laporan/kunjungan') ?>">
										<span class="sub-item">Laporan Kunjungan</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url('laporan/pembayaran') ?>">
										<span class="sub-item">laporan Pembayaran</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url('laporan/riwayat') ?>">
										<span class="sub-item">laporan Riwayat Pasien</span>
									</a>
								</li>
							</ul>
						</div>
					</li>
					<li class="nav-item">
						<a data-toggle="collapse" href="#preference">
							<i class="flaticon-database"></i>
							<p>Data Preference</p>
							<span class="caret"></span>
						</a>
						<div class="collapse" id="preference">
							<ul class="nav nav-collapse">
								<li>
									<a href="<?php echo base_url('data/backup') ?>">
										<span class="sub-item">Backup Data</span>
									</a>
								</li>
								<li>
									<a href="<?php echo base_url('data/restore') ?>">
										<span class="sub-item">Restore Data</span>
									</a>
								</li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
<?php } ?>

<?php if ($this->session->userdata('level') == '2') { ?>
	<div class="sidebar sidebar-style-2" data-background-color="white">
		<div class="sidebar-wrapper scrollbar scrollbar-inner">
			<div class="sidebar-content">
				<ul class="nav nav-primary">
					<li class="nav-item active">
						<a>
							<i class="fas fa-user"></i>
							<p>Menu <?php echo text_level($this->session->userdata('level'))?></p>
						</a>
					</li>
				</ul>
				<ul class="nav nav-primary">
					<li class="nav-section">
						<span class="sidebar-mini-icon">
							<i class="fa fa-ellipsis-h"></i>
						</span>
						<h4 class="text-section">Pilihan</h4>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('pasien') ?>">
							<i class="fas fa-user"></i>
							<p>Pasien</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('kunjungan') ?>">
							<i class="fas fa-stethoscope"></i>
							<p>Kunjungan</p>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
<?php } ?>

<?php if ($this->session->userdata('level') == '3') { ?>
	<div class="sidebar sidebar-style-2" data-background-color="white">
		<div class="sidebar-wrapper scrollbar scrollbar-inner">
			<div class="sidebar-content">
				<ul class="nav nav-primary">
					<li class="nav-item active">
						<a>
							<i class="fas fa-user"></i>
							<p>Menu <?php echo text_level($this->session->userdata('level'))?></p>
						</a>
					</li>
				</ul>
				<ul class="nav nav-primary">
					<li class="nav-section">
						<span class="sidebar-mini-icon">
							<i class="fa fa-ellipsis-h"></i>
						</span>
						<h4 class="text-section">Pilihan</h4>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('obatmasuk') ?>">
							<i class="fas fa-briefcase-medical"></i>
							<p>Obat Masuk</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('obat') ?>">
							<i class="fas fa-capsules"></i>
							<p>Obat</p>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
<?php } ?>

<?php if ($this->session->userdata('level') == '4') { ?>
	<div class="sidebar sidebar-style-2" data-background-color="white">
		<div class="sidebar-wrapper scrollbar scrollbar-inner">
			<div class="sidebar-content">
				<ul class="nav nav-primary">
					<li class="nav-item active">
						<a>
							<i class="fas fa-user"></i>
							<p>Menu <?php echo text_level($this->session->userdata('level'))?></p>
						</a>
					</li>
				</ul>
				<ul class="nav nav-primary">
					<li class="nav-item">
						<a href="<?php echo base_url('pembayaran') ?>">
							<i class="fas fa-prescription"></i>
							<p>Pembayaran (Blm Lunas)</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('pembayaran/lunas') ?>">
							<i class="fas fa-prescription"></i>
							<p>Pembayaran Lunas</p>
						</a>
					</li>
					<li class="nav-item" hidden>
						<a href="">
							<i class="fas fa-desktop"></i>
							<p>Notifikasi Stock</p>
							<span class="badge badge-danger">4</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
<?php } ?>

<?php if ($this->session->userdata('level') == 'Dokter') { ?>
	<div class="sidebar sidebar-style-2" data-background-color="#white">
		<div class="sidebar-wrapper scrollbar scrollbar-inner">
			<div class="sidebar-content">
				<ul class="nav nav-primary">
					<li class="nav-item active">
						<a>
							<i class="fas fa-user"></i>
							<p>Menu <?php echo text_level($this->session->userdata('level'))?></p>
						</a>
					</li>
				</ul>
				<ul class="nav nav-primary">
					<li class="nav-item">
						<a href="<?php echo base_url('periksa') ?>">
							<i class="fas fa-users"></i>
							<p>Antrean</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo base_url('periksa/daftar_selesai_periksa') ?>">
							<i class="fas fa-medkit"></i>
							<p>Selesai Periksa</p>
						</a>
					</li>
					<li class="nav-item" hidden>
						<a href="">
							<i class="fas fa-desktop"></i>
							<p>Notifikasi Stock</p>
							<span class="badge badge-danger">4</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
<?php } ?>
