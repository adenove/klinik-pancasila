<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Input <?php echo $title ?></h4>
				</div>
				<div class="card-body">
					<form id="" method="post" action="<?php echo base_url($action) ?>">
						<div class="card-body">
							<div hidden><?php input_text("ID","text","","id_pasien",$arrayIdPasien[0],"","","")?></div>
							<?php input_text("Nama Pasien","text","","pasien",$arrayNamaPasien[0],"","readonly required","")?>
							<!-- <?php input_option("Nama Pasien","pasien"," - ",$arrayIdPasien,$arrayNamaPasien,"id_pasien",$id_pasien,"","required","") ?> -->
							<?php input_option("Jenis Kunjungan","select"," - ",[1,2,3,4,5],[text_jeniskunjungan(1),text_jeniskunjungan(2),text_jeniskunjungan(3),text_jeniskunjungan(4),text_jeniskunjungan(5)],"jenis_kunjungan",$jenis_kunjungan,"required","") ?>
							<?php input_text("Keluhan","text","","keluhan",$keluhan,"","required","")?>
							<?php input_option("Nama Dokter","select3"," - ",$arrayIdDokter,$arrayNamaDokter,"id_dokter",$id_dokter,"","required","") ?>
						</div>
						<div class="card-action">
							<?php input_button() ?>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
