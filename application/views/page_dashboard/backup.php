<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title"><?php echo $title ?></h4>
				</div>
				<div class="card-body">
					<a class="btn btn-info" href="<?php echo base_url('data/backupdb') ;?>">Download Backup</a>
				</div>
			</div>
		</div>
	</div>
</div>
