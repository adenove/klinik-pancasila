<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title"><?php echo $title ?></h4>
				</div>
				<div class="card-body">
				<form method="post" action="<?php echo base_url($action)?>" enctype="multipart/form-data">
					<input type="file" id="profile_image" name="profile_image" size="33" />
					<hr>
					<div class="d-flex">
						<button type="submit" class="btn btn-primary">upload</button>
						<?php if($errormsg != ""){ ;?>
							<div class="alert alert-success m-0 ml-3" role="alert">
							<?php echo $errormsg ;?>
						</div>
						<?php };?>
					</div>
            	</form>
				</div>
			</div>
		</div>
	</div>
</div>
