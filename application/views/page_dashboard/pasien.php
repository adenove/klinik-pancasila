<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Daftar <?php echo $title ?></h4>
				</div>
				<div class="card-body">
					<?php echo button_add($title,'href="'.base_url('pasien/input').'"') ?><br><br>
					<div class="table-responsive">
						<table id="basic-datatables" class="display table table-striped table-hover" >
							<thead>
								<tr>
									<th>No</th>
									<th>No RM</th>
									<th>Nama</th>
									<th>Tgl Lahir</th>
									<th>Usia</th>
									<th>Agama</th>
									<th>JK</th>
									<th>Alamat</th>
									<th>Orang Tua</th>
									<th>Status</th>
									<th>Pendidikan</th>
									<th>Pekerjaan</th>
									<th>Jenis</th>
									<th>Pilihan</th>
									<th>Kunjungan</th>
									<th>Card</th>
									<th>Info Pasien</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no=1;
									foreach ($data as $d):
									$lahir = new DateTime($d->tanggal_lahir);
							        $today = new DateTime();
							        $usia = $today->diff($lahir);
								?>
								<tr>
									<td><?php echo $no++ ?></td>
									<td><?php echo $d->nomor_rekam_medis ?></td>
									<td><?php echo $d->nama_pasien ?></td>
									<td><?php echo tanggal($d->tanggal_lahir) ?></td>
									<td><?php echo $d->usia // y adalah year ?></td>
									<td><?php echo text_agama($d->agama) ?></td>
									<td><?php echo text_gender($d->jenis_kelamin) ?></td>
									<td><?php echo $d->alamat.', '.$d->kelurahan.', '.$d->kecamatan.', '.$d->kabupaten.', '.$d->provinsi; ?></td>
									<td><?php echo $d->nama_orangtua; ?></td>
									<td><?php echo text_statuspasien($d->status) ?></td>
									<td><?php echo text_pendidikan($d->pendidikan) ?></td>
									<td><?php echo text_pekerjaan($d->pekerjaan) ?></td>
									<td><?php echo text_jenispasien($d->jenis_pasien) ?></td>
									<td width="20%" align="center"><?php echo button_ubah(base_url("pasien/edit/".en($d->id_pasien))).' '.button_delete("pasien/delete/".en($d->id_pasien))?></td>
									<td><?php echo button_antri('Daftar Antri','href="'.base_url('kunjungan/input/'.en($d->id_pasien)).'"');?></td>
									<td width="20%" align="center"><?php echo button_cetak('Cetak Kartu','href="'.base_url('pasien/kartu/'.en($d->id_pasien)).'"') ?></td>
									<td width="20%" align="center"><?php echo button_cetak('Cetak Pasien','href="'.base_url('pasien/info/'.en($d->id_pasien)).'"') ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
