<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="card-title"><?php echo $title ?></div>
				</div>
				<div class="card-body">
					<form id="" method="post" action="">
						<div class="card-body">
							<?php input_text("Nama Petugas","text","","nama_petugas","","","required","") ?>
							<?php input_radio("Jenis Kelamin",array("L","P"),array("Laki-laki","Perempuan"),"jeniskelamin","","required","") ?>
							<?php input_option("Provinsi","select"," - ",array("jateng","diy","jatim"),array("Jawa Tengah","DIY","Jawa Timur"),"provinsi","","required","") ?>
							<?php input_date("Tanggal Lahir","text","date","taggal_lahir","","required","") ?>
							<div class="separator-solid"></div>
						</div>
						<div class="card-action">
							<?php input_button() ?>
						</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
