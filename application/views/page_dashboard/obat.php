<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Daftar <?php echo $title ?></h4>
				</div>
				<div class="card-body">
					<?php echo button_add($title,'href="'.base_url('obat/input').'"') ?><br><br>
					<div class="table-responsive">
						<table id="basic-datatables" class="display table table-striped table-hover" >
							<thead>
								<tr>
									<th>No</th>
									<th>Nama</th>
									<th>Harga Jual</th>
									<th>Stok</th>
									<th>Pilihan</th>
								</tr>
							</thead>
							<tbody>
								<?php 
	  								$no=1;
	  								foreach ($data as $d):
  								?>
								<tr>
									<td width="10%"><?php echo $no++ ?></td>
									<td><?php echo $d->nama_obat ?></td>
									<td><?php echo rupiah($d->harga_jual) ?></td>
									<td><?php echo $d->stok_obat ?></td>
									<td width="20%" align="center"><?php echo button_ubah(base_url("obat/edit/".en($d->id_obat))).' '.button_delete("obat/delete/".en($d->id_obat))?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
