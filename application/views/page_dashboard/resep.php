<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card card-invoice">
				<div class="card-header">
					<div class="invoice-header">
						<h3 class="invoice-title">
							Resep
						</h3>
						<div class="logo">
							<img src="<?php echo base_url() ?>assets/img/logo_text.png" alt="">
						</div>
					</div>
					<div class="invoice-desc">
						Jl. Wonogiri-Pacitan, Jang Lot, Baturetno,<br/>
						Kabupaten Wonogiri, Jawa Tengah 57673
					</div>
				</div>
				<div class="card-header">
					<div class="invoice-desc">
						<form id="isform" method="post" action="<?php echo base_url('periksa/create_periksa') ?>">
							<div hidden><?php input_text("Jumlah","text","","id_kunjungan",$id_kunjungan,"","required","")?></div>
							<div><?php input_optione("ICD & Diagnosa","icd"," - ",$idDiagnosa,$icd_Diagnosa,"icd",$icd,"","required","") ?></div>
							<div hidden><?php input_text("Diagnosa","text","diagnosa","diagnosa","","","required","")?></div>
							<div><?php input_text("Tindakan","text","","tindakan","","","required","")?></div>
						</form>
					</div>
				</div>
				<div class="card-body">
					<div class="separator-solid"></div>

					<?php
						foreach ($dataKunjungan as $d):
							if ($d->id_dokter !== $this->session->userdata('id'))
								{ $show = '';}
							else {
					?>
					<div class="row">
						<div class="col-md-4 info-invoice">
							<h5 class="sub">No. Resep</h5>
							<p>RSP000<?php echo $d->id_kunjungan ?></p>
						</div>
						<div class="col-md-4 info-invoice">
							<h5 class="sub">Tgl Berobat</h5>
							<p><?php echo tanggal($d->tanggal_kunjungan) ?></p>
							<h5 class="sub">Keluhan</h5>
							<p><?php echo $d->keluhan ?></p>
						</div>
						<div class="col-md-4 info-invoice">
							<h5 class="sub">Resep untuk</h5>
							<p>
								<?php echo $d->nama_pasien ?><br/>
								<?php echo $d->alamat ?><br/>
								<?php echo $d->kecamatan.', '.$d->kabupaten ?>
							</p>
						</div>
					</div>
					<?php } endforeach; ?>

					<div class="row">
						<div class="col-md-12">
							<div class="invoice-detail">
								<div class="invoice-top">
									<h3 class="title"><strong></strong></h3>
								</div>
								<div class="invoice-item">
									<div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr>
													<td><strong>No</strong></td>
													<td class="text-left"><strong>Obat</strong></td>
													<td class="text-left"><strong>Keterangan</strong></td>
													<td class="text-center"><strong>Qty</strong></td>
													<td class="text-center"><strong>Pilihan</strong></td>
												</tr>
											</thead>
											<tbody>
											<?php
												$no=1;
												foreach ($dataResep as $d):

											?>
												<tr>
													<td><?php echo $no++ ?></td>
													<td class="text-left"><?php echo $d->nama_obat ?></td>
													<td class="text-justify"><?php echo $d->keterangan ?></td>
													<td class="text-center"><?php echo $d->jumlah_obat_keluar ?></td>
													<td width="20%" align="center"><?php echo button_delete( base_url()."periksa/delete/".en($resep)."/".en($d->id_resep))?></td>
												</tr>
											<?php endforeach; ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="separator-solid  mb-3"></div>
							<?php echo button_add('Obat','data-toggle="modal" data-target="#input"') ?>
							<!-- Button trigger modal -->
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
								Cek Stock Obat
							</button>
						</div>
					</div>
				</div>
				<div class="card-footer">
					<div class="row">
						<div class="col-sm-7 col-md-5 mb-3 mb-md-0 transfer-to">
						</div>
						<div class="col-sm-5 col-md-7 transfer-total">
						</div>
					</div>
					<div class="separator-solid"></div>
					<div class="text-right">
						<button type="submit" form="isform" class="btn btn-primary" >
							Selesai
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal Input-->
<div class="modal fade" id="input" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Input Obat</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="" method="post" action="<?php echo base_url('periksa/create_resep') ?>">
					<div class="card-body">
						<div hidden><?php input_text("Jumlah","text","","id_kunjungan",$id_kunjungan,"","required","")?></div>
						<?php input_option("Nama Obat","select2"," - ",$arrayIdObat,$arrayNamaObat,"id_obat","","required","") ?>
						<?php input_text("Jumlah","text","","jumlah_obat_keluar","","","required","")?>
						<?php input_text("Keterangan","text","","keterangan","","","required","")?>
					</div>
					<div class="card-action">
						<?php input_button() ?>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /Modal Input-->

<!-- Modal Edit-->
<div class="modal fade" id="resep_modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Input Obat</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="" method="post" action="<?php echo base_url('periksa/edit_resep') ?>">
					<div class="card-body">
						<div><?php input_text("ID RESEP","text","id_resep","id_resep","","","required","")?></div>
						<div hidden><?php input_text("Jumlah","text","","id_kunjungan",$id_kunjungan,"","required","")?></div>
						<?php input_option1("Nama Obat","select"," - ",$arrayIdObat,$arrayNamaObat,"id_obat","","required","") ?>
						<?php input_text("Jumlah","text","jumlah","jumlah_obat_keluar","","","required","")?>
						<?php input_text("Keterangan","text","keterangan","keterangan","","","required","")?>
					</div>
					<div class="card-action">
						<?php input_button() ?>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /Modal Input-->

<!-- Modal -->
<div class="modal fade " id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">Stock Obat</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
						<table id="basic-datatables" class="display table table-striped table-hover" >
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Obat</th>
									<th>Harga Jual</th>
									<th>Stock</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no=1;
									foreach ($stockobat as $d):
								?>
								<tr>
									<td><?php echo $no++ ?></td>
									<td><?php echo $d->nama_obat ?></td>
									<td><?php echo $d->harga_jual; ?></td>
									<td><?php echo $d->stok_obat; ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>