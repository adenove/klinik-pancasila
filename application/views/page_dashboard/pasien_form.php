<div class="page-inner">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title"><?php echo $title ?></h4>
				</div>
				<div class="card-body" id="section-to-print" >
					<form id="" method="post" action="<?php echo base_url($action) ?>">
						<div class="card-body">
							<div hidden><?php input_text("ID","text","","id",$id_pasien,"","readonly required","")?></div>
							<?php input_text("No Rekam Medis","text","","nomor_rekam_medis",$nomor_rekam_medis,"","readonly required","")?>
							<?php input_text("Nama Pasien","text","","nama_pasien",$nama_pasien,"","required","")?>
							<?php input_date("Tanggal Lahir","text","date","tanggal_lahir",$tanggal_lahir,"required","") ?>
							<div hidden><?php input_text("Usia","text","usia","usia",$usia,"","","")?></div>
							<?php input_option("Agama","select"," - ",[1,2,3,4,5,6],[text_agama(1),text_agama(2),text_agama(3),text_agama(4),text_agama(5),text_agama(6)],"agama",$agama,"required","") ?>
							<?php input_radio("Jenis Kelamin",array("L","P"),array(text_gender("L"),text_gender("P")),"jenis_kelamin",$jenis_kelamin,"required","") ?>
							<?php input_text("Alamat","text","","alamat",$alamat,"","required","")?>
							<?php input_option("Nama Provinsi","provinsi"," - ",[],[],"provinsi",$provinsi,"required","") ?>
							<?php input_option("Nama Kabupaten","kabupaten"," - ",[],[],"kabupaten",$id_kabupaten,"required","") ?>
							<?php input_option("Nama Kecamatan","kecamatan"," - ",[],[],"kecamatan",$kecamatan,"required","") ?>
							<?php input_option("Nama Kelurahan","kelurahan"," - ",[],[],"kelurahan",$kelurahan,"required","") ?>
							<?php input_text("Nama Orang Tua","text","","nama_orangtua",$nama_orangtua,"","required","")?>
							<?php input_option("Status","select4"," - ",array(1,2,3,4),array(text_statuspasien(1),text_statuspasien(2),text_statuspasien(3),text_statuspasien(4)),"status",$status,"required","") ?>
							<?php input_option("Pendidikan","select5"," - ",array(1,2,3,4,5,6),array(text_pendidikan(1),text_pendidikan(2),text_pendidikan(3),text_pendidikan(4),text_pendidikan(5),text_pendidikan(6)),"pendidikan",$pendidikan,"required","") ?>
							<?php input_option("Pekerjaan","select6"," - ",array(1,2,3,4,5,6,7,8),array(text_pekerjaan(1),text_pekerjaan(2),text_pekerjaan(3),text_pekerjaan(4),text_pekerjaan(5),text_pekerjaan(6),text_pekerjaan(7),text_pekerjaan(8)),"pekerjaan",$pekerjaan,"required","") ?>
							<?php input_option("Jenis Pasien","select7"," - ",array(1,2,3),array(text_jenispasien(1),text_jenispasien(2),text_jenispasien(3)),"jenis_pasien",$jenis_pasien,"required","") ?>
							<?php input_option("Jenis Kunjungan","jenis_kunjungan"," - ",[1,2,3,4,5],[text_jeniskunjungan(1),text_jeniskunjungan(2),text_jeniskunjungan(3),text_jeniskunjungan(4),text_jeniskunjungan(5)],"jenis_kunjungan",$jenis_kunjungan,"required","") ?>
							<?php input_text("Keluhan","text","","keluhan",$keluhan,"","required","")?>
							<?php input_option("Nama Dokter","select3"," - ",$arrayIdDokter,$arrayNamaDokter,"id_dokter",$id_dokter,"","required","") ?>
						</div>
						<div class="card-action">
							<?php input_button() ?>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
