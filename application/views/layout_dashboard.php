<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title><?php echo $title ?></title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="<?php echo base_url() ?>assets/img/icon.ico" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="<?php echo base_url() ?>assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['<?php echo base_url() ?>assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>
	<!-- CSS Files -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/atlantis.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/plugin/datepicker/bootstrap-datepicker.js">
	<!-- Chart JS -->
	<script src="<?php echo base_url() ?>assets/js/plugin/chart.js/chart.min.js"></script>

	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/print.css">

</head>
<body>
	<div class="wrapper">
		<div class="main-header">
			<!-- Logo Header -->

			<!-- End Logo Header -->

			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg" data-background-color="">
				<div class="container-fluid">
						<img src="<?php echo base_url() ?>assets/img/logo.png" width="35" alt="navbar brand" class="navbar-brand">
						<h6 class="card-title"><strong>KLINIK PANCASILA BATURETNO</strong></h6>
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item dropdown hidden-caret">
							<a hidden class="nav-link dropdown-toggle" href="#" id="messageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-bell"></i>
								<span class="notification">4</span>
							</a>
							<ul class="dropdown-menu messages-notif-box animated fadeIn" aria-labelledby="messageDropdown">
								<li>
									<div class="dropdown-title d-flex justify-content-between align-items-center">
										Messages
										<a href="#" class="small">Mark all as read</a>
									</div>
								</li>
								<li>
									<div class="message-notif-scroll scrollbar-outer">
										<div class="notif-center">
											<a href="#">
												<div class="notif-img">
													<img src="<?php echo base_url() ?>assets/img/jm_denis.jpg" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="subject">Jimmy Denis</span>
													<span class="block">
														How are you ?
													</span>
													<span class="time">5 minutes ago</span>
												</div>
											</a>
											<a href="#">
												<div class="notif-img">
													<img src="<?php echo base_url() ?>assets/img/chadengle.jpg" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="subject">Chad</span>
													<span class="block">
														Ok, Thanks !
													</span>
													<span class="time">12 minutes ago</span>
												</div>
											</a>
											<a href="#">
												<div class="notif-img">
													<img src="<?php echo base_url() ?>assets/img/mlane.jpg" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="subject">Jhon Doe</span>
													<span class="block">
														Ready for the meeting today...
													</span>
													<span class="time">12 minutes ago</span>
												</div>
											</a>
											<a href="#">
												<div class="notif-img">
													<img src="<?php echo base_url() ?>assets/img/talha.jpg" alt="Img Profile">
												</div>
												<div class="notif-content">
													<span class="subject">Talha</span>
													<span class="block">
														Hi, Apa Kabar ?
													</span>
													<span class="time">17 minutes ago</span>
												</div>
											</a>
										</div>
									</div>
								</li>
								<li>
									<a class="see-all" href="javascript:void(0);">See all messages<i class="fa fa-angle-right"></i> </a>
								</li>
							</ul>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
								<div class="avatar-sm">
									<img src="<?php echo base_url() ?>assets/img/profile.jpg" alt="..." class="avatar-img rounded-circle">
								</div>
							</a>
							<ul class="dropdown-menu dropdown-user animated fadeIn">
								<div class="dropdown-user-scroll scrollbar-outer">
									<li>
										<div class="user-box">
											<div class="avatar-lg"><img src="<?php echo base_url() ?>assets/img/profile.jpg" alt="image profile" class="avatar-img rounded"></div>
											<div class="u-text">
												<h4><?php echo $this->session->userdata('nama')?></h4>
												<p class="text-muted"><?php echo text_level($this->session->userdata('level'))?></p>
												<a href="" class="btn btn-xs btn-secondary btn-sm">Edit</a>
											</div>
										</div>
									</li>
									<li>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">Setting</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="<?php echo base_url() ?>auth/logout">Logout</a>
									</li>
								</div>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>
		<!-- Sidebar -->
		<?php $this->load->view('page_dashboard/menu'); ?>
		<!-- /Sidebar -->
		<div class="main-panel">
			<div class="content">
				<!-- Breadcrumbs-->
				<ol class="breadcrumb">
					<?php foreach ($this->uri->segments as $segment): ?>
						<?php
							$url = substr($this->uri->uri_string, 0, strpos($this->uri->uri_string, $segment)) . $segment;
							$is_active =  $url == $this->uri->uri_string;
						?>


						<li class="breadcrumb-item <?php echo $is_active ? 'active': '' ?>">
							<?php if($is_active): ?>
								<?php echo ucfirst($segment) ?>
							<?php else: ?>
								<a href="<?php echo site_url($url) ?>"><?php echo ucfirst($segment) ?></a>
							<?php endif; ?>
						</li>
					<?php endforeach; ?>
				</ol>
				<?php $this->load->view('page_dashboard/'.$page); ?>
			</div>
			<footer class="footer">
				<div class="container-fluid">
					<nav class="pull-left">
						<ul class="nav">
							<li class="nav-item">
								<a class="nav-link" href="">
									Documentation
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">
									Help
								</a>
							</li>
						</ul>
					</nav>
					<div class="copyright ml-auto">
						<?php echo copyright(); ?>
					</div>
				</div>
			</footer>
		</div>
	</div>
	<!--   Core JS Files   -->
	<script src="<?php echo base_url() ?>assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/core/popper.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/core/bootstrap.min.js"></script>
	<!-- jQuery UI -->
	<script src="<?php echo base_url() ?>assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
	<!-- jQuery Scrollbar -->
	<script src="<?php echo base_url() ?>assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
	<!-- DateTimePicker -->
	<script src="<?php echo base_url() ?>assets/js/plugin/datepicker/bootstrap-datepicker.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/plugin/datepicker/bootstrap-datepicker3.css"/>
	<!-- Select2 -->
	<script src="<?php echo base_url() ?>assets/js/plugin/select2/select2.full.min.js"></script>
	<!-- Datatables -->
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/datatables.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.bootstrap.jss"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.buttons.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/buttons.bootstrap.min.jss"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/jszip.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/pdfmake.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/vfs_fonts.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/buttons.html5.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/buttons.print.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.fixedHeader.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.keyTable.min.jss"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.responsive.min.jss"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/responsive.bootstrap.min.jss"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.scroller.min.js"></script>
	<!-- Chart JS -->
	<script src="<?php echo base_url() ?>assets/js/plugin/chart.js/chart.min.js"></script>
	<!-- Atlantis JS -->
	<script src="<?php echo base_url() ?>assets/js/atlantis.min.js"></script>

	<script>
		// In your Javascript (external .js resource or <script> tag)
		$(document).ready(function() {
		    $('#date').datepicker({
				format: 'dd-mm-yyyy',
				theme: 'bootstrap'
			});
			$('#date2').datepicker({
				format: 'dd-mm-yyyy',
				theme: 'bootstrap'
			});

	        //select2
			$('#select').select2({
		    	theme: 'bootstrap',
		    	width: '200px'
		    });
		    $('#select2').select2({
		    	theme: 'bootstrap',
		    	width: '200px'
		    });
		    $('#select3').select2({
		    	theme: 'bootstrap',
		    	width: '200px'
		    });
		    $('#select4').select2({
		    	theme: 'bootstrap',
		    	width: '200px'
		    });
		    $('#select5').select2({
		    	theme: 'bootstrap',
		    	width: '200px'
		    });
		    $('#select6').select2({
		    	theme: 'bootstrap',
		    	width: '200px'
		    });
		    $('#select7').select2({
		    	theme: 'bootstrap',
		    	width: '200px'
		    });
		    $('#kecamatan').select2({
		    	theme: 'bootstrap',
		    	width: '200px'
			});
			$('#provinsi').select2({
				theme: "bootstrap",
				width: '200px'
			});
			$('#jenis_kunjungan').select2({
				theme: "bootstrap",
				width: '200px'
			});
			$('#kabupaten').select2({
				theme: "bootstrap",
				width: '200px'
			});
			$('#kelurahan').select2({
				theme: "bootstrap",
				width: '200px'
			});
			$('#icd').select2({
				theme: "bootstrap",
				width: '200px'
			});
		});
	</script>
	<script >
		$(document).ready(function() {
			$('#basic-datatables').DataTable({
				dom: 'Bfrtip',
				buttons: [{
		          extend: "excel",
		          text: "<i class='fas fa-file-excel'></i> Export Excel",
		          className: "btn btn-info btn-border btn-xs"
		        }]
			});
		});
	</script>
	<script type="text/javascript">
		//chek Kecamatan
		path = window.location.pathname;
		if(path.search('kecamatan') > 0){
			$.getJSON('<?php echo base_url('province.json');?>',function(data){
				$.each(data,function(k,v){
						$('#provinsi').append(`<option>${v.province}</option>`);
				});

				$('#provinsi').change(function(){
					var isdef  = $(this).val();
					$('#kabupaten').html("");
					$.each(data,function(k,v){
						if(isdef == v.province ){
							$.each(v.city,function(i,l){
								$('#kabupaten').append(`<option>${l.city_name}<option>`);
							});
						}
					});
				});
			});
		}


	    $(document).ready(function(){
	        $('#select2').change(function(){
	            var id=$(this).val();
	            $.ajax({
	                url : "<?php echo base_url();?>pasien/get_kecamatan",
	                method : "POST",
	                data : {id: id},
	                async : false,
	                dataType : 'json',
	                success: function(data){
	                    var html = '';
	                    var i;
	                    for(i=0; i<data.length; i++){
	                        html += '<option value="'+data[i].id_kecamatan+'">'+data[i].nama_kecamatan+'</option>';
	                    }
	                    $('#select3').html(html);

	                }
	            });
	        });
	    });
	</script>
	<script type="text/javascript" >

		function get_kelurahan(){
			// kelurahan
			var kecamatan = $('#kecamatan').val();
			$('#kelurahan').html("");
			$.getJSON("<?php echo base_url('kelurahan.json');?>",function(data){
				$.each(data,function(k,v){
					if(v.kecamatan == kecamatan)
					{
						$('#kelurahan').append(`<option>${v.kelurahan}</option>`);
					}
				});
			});
		}


		$(document).ready(function(){
			var setProv = "";

			//chek is pasien edit
			path = window.location.pathname;
			if(path.search('pasien/edit') > 0){
				setProv = "<?php echo $provinsi;?>";
			}


			$.getJSON('<?php echo base_url('province.json');?>',function(data){

				$.each(data,function(k,v){
					if(setProv == v.province){
						$('#provinsi').append(`<option selected >${v.province}</option>`);
					}else{
						$('#provinsi').append(`<option>${v.province}</option>`);
					}
				});

				if(setProv != ""){
					$('#kabupaten').html("");
					var setKabupaten = '<?php echo $kabupaten;?>';
					$.each(data,function(k,v){
						if(setProv == v.province){
							$.each(v.city,function(i,l){
								if(setKabupaten == l.city_name){
									$('#kabupaten').append(`<option selected>${l.city_name}<option>`);
								}else{
									$('#kabupaten').append(`<option>${l.city_name}<option>`);
								}
							});
						}
					});
				}

				$('#provinsi').change(function(){
					var isdef  = $(this).val();
					$('#kabupaten').html("");
					$.each(data,function(k,v){
						if(isdef == v.province ){
							$.each(v.city,function(i,l){
								$('#kabupaten').append(`<option>${l.city_name}<option>`);
							});
						}
					});
					var kabs = $("#kabupaten").val();
					if(kabs != ""){
						$.getJSON("<?php echo base_url('csvjson.json');?>",function(data){
							$("#kecamatan").html("");
							$.each(data,function(k,v){
								if(v.kabupaten == kabs){
									$('#kecamatan').append(`<option>${v.kecamatan}<option>`);
								};
							});

							// kelurahan
							get_kelurahan();

						});
					}
				});

				$('#kabupaten').change(function(){
					$kab = $(this).val();
					$.getJSON("<?php echo base_url('csvjson.json');?>",function(data){
						$("#kecamatan").html("");
						var isparse = data;
						$.each(isparse,function(k,v){
							if(v.kabupaten == $kab){
								$('#kecamatan').append(`<option>${v.kecamatan}<option>`);
							};
						});
						get_kelurahan();
					});
				});

				$('#kecamatan').change(function(){
					get_kelurahan();
				});

				//edit
				$.get('<?php echo base_url('csvjson.json');?>',function(data){
					var kab = $('#kabupaten').val();
					var setkec = '<?php echo $kecamatan;?>';
					$("#kecamatan").html("");
					if(setkec != ""){
						$.each(data,function(k,v){
							if(kab == v.kabupaten){
								if(v.kecamatan == setkec){
									$('#kecamatan').append(`<option selected>${v.kecamatan}<option>`);
								}else{
									$('#kecamatan').append(`<option>${v.kecamatan}<option>`);
								}
							}
						});
					}

					// getkelurahan
					$.getJSON("<?php echo base_url('kelurahan.json');?>",function(data){
						$("#kelurahan").html("");
						var iskelurahan = '<?php echo $kelurahan;?>';
						$.each(data,function(k,v){
							if(v.kecamatan == setkec){
								if(v.kelurahan == iskelurahan){
									$('#kelurahan').append(`<option selected >${v.kelurahan}<option>`);
								}else{
									$('#kelurahan').append(`<option>${v.kelurahan}<option>`);
								}
							};
						});
					});


				});
			});

		});
	</script>
	<script>
		$(document).ready(function(){
			$('#print_k').click(function(){
				var curURL = window.location.href;
				history.replaceState(history.state, '', '/');
				window.print();
				history.replaceState(history.state, '', curURL);
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			$('button#resep_e').click(function(){
				var id = $(this).data('id');
				$('#resep_modal').modal('show');
				$.post('<?php echo base_url('/periksa/edit');?>',{id:id}).done(function(data){
					var parse = JSON.parse(data);
					var id_obat = '<?php echo json_encode($arrayIdObat) ;?>';
					var nama_obat = '<?php echo json_encode($arrayNamaObat) ;?>';
					var parse_id_obat = JSON.parse(id_obat);
					var parse_nama_obat = JSON.parse(nama_obat);
					$('#select').html("");
					for (let i = 0; i < parse_id_obat.length; i++) {
						if(parse[0]['id_obat'] == parse_id_obat[i]){
							$('#select').append(`<option value=${parse_id_obat[i]} selected >${parse_nama_obat[i]}</option>`);
						}else
						{
							$('#select').append(`<option value=${parse_id_obat[i]} >${parse_nama_obat[i]}</option>`);
						}
					}

					$('#id_resep').val(parse[0]['id_resep']);
					$('#jumlah').val(parse[0]['jumlah_obat_keluar']);
					$('#keterangan').val(parse[0]['keterangan']);
				});
			});
		});
	</script>
	<script>
		$(document).ready(function(){
			$('.editicd').click(function(){
				var id = $(this).data('id');
				$.post('<?php echo base_url();?>diagnosa/edit/'+id,{id:id}).done(function(data){
					var parse = JSON.parse(data);
					$('#id_icd').val(id);
					$('#icd').val(parse[0]['icd']);
					$('#diagnosa').val(parse[0]['diagnosa']);
				});
				$('#editdiagnosa').modal('show');
			});
		});
	</script>
	<script>
		$("#icd").change(function(){
			var icd = $('#icd').val();
			$.post('<?php echo base_url();?>diagnosa/getdiagnosa',{icd:icd}).done(function(data){
				var diagnosa = JSON.parse(data);
				if(icd != ""){
					$('#diagnosa').val(diagnosa[0].diagnosa);
				}else{
					$('#diagnosa').val("");
				}
			});
		});
	</script>
</body>
</html>
