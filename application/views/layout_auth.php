<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Login</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="<?php echo base_url() ?>assets/img/icon.ico" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="<?php echo base_url() ?>assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['../assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/atlantis.min.css">
</head>
<body class="login">
<div class="p-0" style="background-image: url('<?php echo base_url()?>assets/img/build.jpeg');display:flex;justify-content:center;align-items:center;position: absolute;width: 100%;height: 100%;" >
		<div class="login-aside w-25 d-flex align-items-center justify-content-center bg-white">
			<div class="container container-login container-transparent animated fadeIn">
				<p class="text-center"><img src="<?php echo base_url() ?>assets/img/logo.png" alt="" width="100"></p>
				<h3 class="text-center">Sistem informasi pendaftaran klinik pratama pancasila</h3>
				<h3 class="text-center">Login</h3>
				<form class="login-form" method="post" action="<?php echo base_url()?>auth/check">
					<div class="form-group">
						<label for="username" class="placeholder"><b>Username</b></label>
						<input name="username" type="text" class="form-control" required autofocus>
					</div>
					<div class="form-group">
						<label for="password" class="placeholder"><b>Password</b></label>
						<div class="position-relative">
							<input id="password" name="password" type="password" class="form-control" required autofocus>
							<div class="show-password">
								<i class="icon-eye"></i>
							</div>
						</div>
					</div>
					<div class="form-group form-action-d-flex mb-3 text-center">
						<button type="submit" class="btn btn-secondary col-md-5 mt-3 mt-sm-0 fw-bold">Masuk</button>
					</div>
					<div class="copyright text-center" style="color:lightgray;" >
						<br><?php echo copyright(); ?>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script src="<?php echo base_url() ?>assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/core/popper.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/core/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/atlantis.min.js"></script>
	<!-- Bootstrap Notify -->
	<script src="<?php echo base_url() ?>assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script>
	<script>
		$(document).ready(function(){
			var placementFrom = 'top';
			var placementAlign = 'right';
			var state = 'danger';
			var style = 'withicon';
			var content = {};

			content.message = 'Kombinasi username dan password salah';
			content.title = 'Gagal!';
			if (style == "withicon") {
				content.icon = 'fa fa-bell';
			} else {
				content.icon = 'none';
			}
			content.url = '';
			content.target = '_blank';

			$.<?php echo $notif ?>(content,{
				type: state,
				placement: {
					from: placementFrom,
					align: placementAlign
				},
				time: 1000,
				delay: 5000,
			});
		});
	</script>
</html>
