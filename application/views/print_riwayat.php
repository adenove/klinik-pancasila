<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $page;?></title>
    <!-- CSS Files -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/atlantis.min.css">
    <style>
    @media print{
        button {
            display:none !important;
        }
    }
    </style>
</head>
<body>
        <div id="print_area" class="col-md-12">
            <br>
            <button type="button" class="btn btn-primary" id="print_k" >Print invoice</button>
            <br>
            <br>
			<div class="card card-invoice section-to-print">
				<div class="card-header">
					<div class="invoice-header">
						<div class="gr">
                            <h3 class="invoice-title">
                                Riwayat Pasien
                            </h3>
                        </div>
						<div class="logo">
							<img src="<?php echo base_url() ?>assets/img/logo_text.png" alt="">
						</div>
					</div>
					<div class="invoice-desc">
						Jl. Wonogiri-Pacitan, Jang Lot, Baturetno,<br/>
						Kabupaten Wonogiri, Jawa Tengah 57673
					</div>
				</div>
				<div class="card-body">
					<div class="separator-solid"></div>

					<?php
						foreach ($data as $d):
					?>
					<div class="row">
						<div class="col-md-4 info-invoice">
							<h5 class="sub">No. RM</h5>
							<p><?php echo $d->nomor_rekam_medis ?></p>
						</div>
						<div class="col-md-4 info-invoice">
							<h5 class="sub">Tgl Berobat</h5>
							<p><?php echo tanggal($d->tanggal_kunjungan) ?></p>
						</div>
						<div class="col-md-4 info-invoice">
							<h5 class="sub">Riwayat untuk</h5>
							<p>
								<?php echo $d->nama_pasien ?><br/>
								<?php echo $d->alamat ?><br/>
								<?php echo $d->kelurahan.', '.$d->kecamatan.', '.$d->kabupaten.', '.$d->provinsi ?>
							</p>
						</div>
					</div>
					<?php endforeach; ?>

					<div class="row">
						<div class="col-md-12">
						<hr>
						<div class="invoice-detail">
								<div class="invoice-top">
									<h3 class="title text-center"><strong>Keterangan Priksa</strong></h3>
								</div>
								<div class="invoice-item">
									<div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr>
													<td><strong>No</strong></td>
													<td class="text-left"><strong>ICD</strong></td>
													<td class="text-left"><strong>Diagnosa</strong></td>
													<td class="text-left"><strong>Tindakan</strong></td>
													<td class="text-center"><strong>Jenis</strong></td>
													<td class="text-right"><strong>Keluhan</strong></td>
													<td class="text-right"><strong>Dokter</strong></td>
												</tr>
											</thead>
											<tbody>
											<?php
												$no=1;
												foreach ($data as $d):
											?>
												<tr>
													<td><?php echo $no++ ?></td>
													<td class="text-justify"><?php echo $d->icd ?></td>
													<td class="text-left"><?php echo $d->diagnosa ?></td>
													<td><?php echo $d->tindakan ?></td>
													<td class="text-center"><?php echo text_jeniskunjungan($d->jenis_kunjungan) ?></td>
													<td class="text-right"><?php echo $d->keluhan ?></td>
													<td class="text-right"><?php echo $d->nama_dokter ?></td>
												</tr>
											<?php endforeach; ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<hr>
							<div class="invoice-detail">
								<div class="invoice-top">
									<h3 class="title text-center"><strong>Biaya Obat</strong></h3>
								</div>
								<div class="invoice-item">
									<div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr>
													<td><strong>No</strong></td>
													<td class="text-left"><strong>Obat</strong></td>
													<td class="text-left"><strong>Keterangan</strong></td>
													<td class="text-left"><strong>Harga Satuan</strong></td>
													<td class="text-center"><strong>Qty</strong></td>
													<td class="text-right"><strong>Totals</strong></td>
												</tr>
											</thead>
											<tbody>
											<?php
												$no=1;
												$jumlah_total = 0;
												foreach ($dataResep as $d):
												$total = $d->harga_jual * $d->jumlah_obat_keluar;
												$jumlah_total = $total + $jumlah_total;
											?>
												<tr>
													<td><?php echo $no++ ?></td>
													<td class="text-left"><?php echo $d->nama_obat ?></td>
													<td class="text-justify"><?php echo $d->keterangan ?></td>
													<td class="text-right"><?php echo rupiah($d->harga_jual) ?></td>
													<td class="text-center"><?php echo $d->jumlah_obat_keluar ?></td>
													<td class="text-right"><?php echo rupiah($total) ?></td>
												</tr>
											<?php endforeach; ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<hr>
							<div class="invoice-detail">
								<div class="invoice-top">
									<h3 class="title text-center"><strong>Biaya Lainnya</strong></h3>
								</div>
								<div class="invoice-item">
									<div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr>
													<td><strong>No</strong></td>
													<td class="text-left"><strong>Layanan</strong></td>
													<td class="text-left"><strong>Biaya</strong></td>
													<td class="text-left"><strong>Jumlah</strong></td>
													<td class="text-right"><strong>Total</strong></td>
												</tr>
											</thead>
											<tbody>
											<?php
												$no=1;
												$jumlah_total2 = 0;
												foreach ($dataBiayaLain as $d):
												$total2 = $d->biaya_layanan * $d->jumlah_layanan;
												$jumlah_total2 = $total2 + $jumlah_total2;
											?>
												<tr>
													<td><?php echo $no++ ?></td>
													<td class="text-left"><?php echo $d->jenis_layanan ?></td>
													<td class="text-justify"><?php echo rupiah($d->biaya_layanan) ?></td>
													<td class="text-justify"><?php echo $d->jumlah_layanan ?></td>
													<td class="text-right"><?php echo rupiah($total2) ?></td>
												</tr>
											<?php endforeach; ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="separator-solid  mb-3"></div>
							<!-- <?php echo button_add('Biaya Lainnya','data-toggle="modal" data-target="#input"') ?> -->
						</div>
					</div>
				</div>
				<div class="card-footer">
				<hr>
					<div class="row">
						<div class="col-sm-7 col-md-5 mb-3 mb-md-0 transfer-to">
						</div>
						<div class="col-sm-5 col-md-7 transfer-total">
							<h5 class="sub">Jumlah Total</h5>
							<div class="price"><?php echo rupiah($jumlah_total+$jumlah_total2) ?></div>
							<span></span>
						</div>
					</div>
					<div class="separator-solid"></div>
					<h6 class="text-uppercase mt-4 mb-3 fw-bold">
						Perhatian
					</h6>
					<p class="text-muted mb-0">
						Terima kasih telah mengurus administrasi pembayaran, semoga lekas sembuh.
					</p>
				</div>
				<div class="card-header">
					<div class="invoice-desc">
						<?php
						if($d->kunjungan_status !== 'resep')
							{ echo '';}
						else { ?>
							<form id="" method="post" action="<?php echo base_url('pembayaran/create') ?>">
								<div class="card-body">
									<div hidden><?php input_text("Jumlah","text","","id_kunjungan",$id_kunjungan,"","required","")?></div>
									<div hidden><?php input_text("Jumlah Transaksi","text","","total_pembayaran",$jumlah_total+$jumlah_total2,"","required","")?></div>
								</div>
								<div class="">
									<!-- <?php button_ok("Bayar","") ?> -->
								</div>
							</form>
							<?php ;}?>
					</div>
				</div>
			</div>
		</div>
    <!--   Core JS Files   -->
	<script src="<?php echo base_url() ?>assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/core/popper.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/core/bootstrap.min.js"></script>
	<!-- jQuery UI -->
	<script src="<?php echo base_url() ?>assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
	<!-- jQuery Scrollbar -->
	<script src="<?php echo base_url() ?>assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
	<!-- DateTimePicker -->
	<script src="<?php echo base_url() ?>assets/js/plugin/datepicker/bootstrap-datepicker.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/plugin/datepicker/bootstrap-datepicker3.css"/>
	<!-- Select2 -->
	<script src="<?php echo base_url() ?>assets/js/plugin/select2/select2.full.min.js"></script>
	<!-- Datatables -->
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/datatables.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.bootstrap.jss"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.buttons.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/buttons.bootstrap.min.jss"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/jszip.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/pdfmake.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/vfs_fonts.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/buttons.html5.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/buttons.print.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.fixedHeader.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.keyTable.min.jss"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.responsive.min.jss"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/responsive.bootstrap.min.jss"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.scroller.min.js"></script>
	<!-- Chart JS -->
	<script src="<?php echo base_url() ?>assets/js/plugin/chart.js/chart.min.js"></script>
	<!-- Atlantis JS -->
    <script src="<?php echo base_url() ?>assets/js/atlantis.min.js"></script>
    <script>
		$(document).ready(function(){
			$('#print_k').click(function(){
				var curURL = window.location.href;
				history.replaceState(history.state, '', '/');
				window.print();
				history.replaceState(history.state, '', curURL);
			});
		});
	</script>
</body>
</html>