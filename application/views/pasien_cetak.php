<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $title;?></title>
    <!-- CSS Files -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/atlantis.min.css">
    <style>

    /* table {
        max-width:50%;
    } */

    th,td {
        padding:15px;
    }

    @media print{
        button {
            display:none !important;
        }
    }
    </style>
</head>
<body>
        <div id="print_area" class="col-md-12">
            <br>
            <button type="button" class="btn btn-primary" id="print_k" >Print</button>
            <br>
            <br>
			<div class="card card-invoice section-to-print">
				<div class="card-header">
					<div class="invoice-header">
						<div class="gr">
                            <h3 class="invoice-title">
                                Pasien
                            </h3>
                        </div>
						<div class="logo">
							<img src="<?php echo base_url() ?>assets/img/logo_text.png" alt="">
						</div>
					</div>
					<div class="invoice-desc">
						Jl. Wonogiri-Pacitan, Jang Lot, Baturetno,<br/>
						Kabupaten Wonogiri, Jawa Tengah 57673
					</div>
                    <hr>
				</div>
				<div class="card-body d-flex justify-content-center">
                <br>
                <br>
                <table>
                    <tr>
                        <th>NO RM</th>
                        <td>: <?php echo $nomor_rekam_medis;?></td>
                    </tr>
                    <tr>
                        <th>Nama</th>
                        <td>: <?php echo $nama_pasien;?></td>
                    </tr>
                    <tr>
                        <th>Tanggal lahir</th>
                        <td>: <?php echo $tanggal_lahir;?></td>
                    </tr>
                    <tr>
                        <th>Usia</th>
                        <td>: <?php echo $usia;?></td>
                    </tr>
                    <tr>
                        <th>Agama</th>
                        <td>: <?php echo text_agama($agama);?></td>
                    </tr>
                    <tr>
                        <th>Jenis kelamin</th>
                        <td>: <?php echo $jenis_kelamin;?></td>
                    </tr>
                    <tr>
                        <th>Alamat</th>
                        <td>: <?php echo $alamat.','.$kecamatan.','.$kabupaten.','.$provinsi?></td>
                    </tr>
                    <tr>
                        <th>Nama Orang Tua</th>
                        <td>: <?php echo $nama_orangtua;?></td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <td>: <?php echo text_statuspasien($status);?></td>
                    </tr>
                    <tr>
                        <th>Pendidikan</th>
                        <td>: <?php echo text_pendidikan($pendidikan);?></td>
                    </tr>
                    <tr>
                        <th>Pekerjaan</th>
                        <td>: <?php echo text_pekerjaan($pekerjaan);?></td>
                    </tr>
                </table>
                  <br>
                  <br>
				</div>
				<div class="card-footer">
					<div class="separator-solid"></div>
					<h6 class="text-uppercase mt-4 mb-3 fw-bold">
						Perhatian
					</h6>
					<p class="text-muted mb-0">
						Terima kasih telah mengurus administrasi pembayaran, semoga lekas sembuh.
					</p>
				</div>

			</div>
		</div>
    <!--   Core JS Files   -->
	<script src="<?php echo base_url() ?>assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/core/popper.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/core/bootstrap.min.js"></script>
	<!-- jQuery UI -->
	<script src="<?php echo base_url() ?>assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
	<!-- jQuery Scrollbar -->
	<script src="<?php echo base_url() ?>assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
	<!-- DateTimePicker -->
	<script src="<?php echo base_url() ?>assets/js/plugin/datepicker/bootstrap-datepicker.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/js/plugin/datepicker/bootstrap-datepicker3.css"/>
	<!-- Select2 -->
	<script src="<?php echo base_url() ?>assets/js/plugin/select2/select2.full.min.js"></script>
	<!-- Datatables -->
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/datatables.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.bootstrap.jss"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.buttons.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/buttons.bootstrap.min.jss"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/jszip.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/pdfmake.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/vfs_fonts.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/buttons.html5.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/buttons.print.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.fixedHeader.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.keyTable.min.jss"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.responsive.min.jss"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/responsive.bootstrap.min.jss"></script>
	<script src="<?php echo base_url() ?>assets/js/plugin/datatables/dataTables.scroller.min.js"></script>
	<!-- Chart JS -->
	<script src="<?php echo base_url() ?>assets/js/plugin/chart.js/chart.min.js"></script>
	<!-- Atlantis JS -->
    <script src="<?php echo base_url() ?>assets/js/atlantis.min.js"></script>
    <script>
		$(document).ready(function(){
			$('#print_k').click(function(){
				var curURL = window.location.href;
				history.replaceState(history.state, '', '/');
				window.print();
				history.replaceState(history.state, '', curURL);
			});
		});
	</script>
</body>
</html>