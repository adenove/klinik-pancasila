<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if ($this->session->userdata('logged_in_user') == FALSE) { reject(); }

		$this->load->model('datarekap');
		$this->load->helper('url');
	}

	public function index()
	{
		$data['title']="Kunjungan Bulan 2019"; //title

		//bulan
		$groupM = array();
		//jumlah
		$groupJ = array();

		$full = array();
		$dataChart = $this->datarekap->kunjungan_bulanan('2019')->result();
		foreach($dataChart as $d){
			if(!in_array(date('M',strtotime($d->tanggal_kunjungan)),$groupM)){
				array_push($groupM,date('M',strtotime($d->tanggal_kunjungan)));
			}
		}

		$update = array();

		foreach($groupM as $m){
			array_push($update,array($m=>0));
		}

		foreach($update as $k => $v){
			foreach($v as $y => $i){
				foreach($dataChart as $d){
					if(date('M',strtotime($d->tanggal_kunjungan)) == $y){
						$update[$k][$y] += 1;
					}
				}
			}
		}

		foreach($update as $k => $v){
			foreach($v as $y => $i){
				array_push($groupJ,$update[$k][$y]);
			}
		}

		$data['labels'] = "Bulan";


		$data['arrayBulan'] = $groupM;
		$data['arrayJumlah'] = $groupJ;

		$data['page'] = "rekap"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}
}
