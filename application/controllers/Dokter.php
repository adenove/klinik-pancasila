<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokter extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if ($this->session->userdata('logged_in_user') == FALSE) { reject(); }

		$this->load->model('datadokter');
		$this->load->helper('url');
	}

	public function index()
	{	
		$data['title']="Dokter"; //title
		$data['data'] = $this->datadokter->daftar()->result();

		$data['page'] = "dokter"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function input()
	{	
		$data['title']="Dokter"; //title

		$data['action'] = "dokter/create";

		$data['xid_dokter'] = "";
		$data['nama_dokter'] = "";
		$data['username_dokter'] = "";
		$data['password_dokter'] = "";

		$data['page'] = "dokter_form"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function create()
	{	
		$data = array(
			'nama_dokter' => $this->input->post('nama'),
			'username_dokter' => $this->input->post('username'),
			'password_dokter' => md5($this->input->post('password'))
		);
		
		$this->datadokter->add($data);
		redirect('dokter');
	}

	public function edit($xid)
	{	
		$id = de($xid);

		$data['title']="Dokter"; //title
		$e = $this->datadokter->edit('id_dokter',$id)->row();

		$data['action'] = "dokter/update";

		$data['xid_dokter'] = $xid;
		$data['nama_dokter'] =  $e->nama_dokter;
		$data['username_dokter'] =  $e->username_dokter;
		$data['password_dokter'] =  $e->password_dokter;

		$data['page'] = "dokter_form"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function update(){

		$xid = $this->input->post('id');
		$id = de($xid);
		
		$data = array(
			'nama_dokter' => $this->input->post('nama'),
			'username_dokter' => $this->input->post('username'),
			'password_dokter' => md5($this->input->post('password'))
		);
		
		$where = array(
			'id_dokter' => $id
		);
		
		$this->datadokter->update($where,$data);
		redirect('dokter');
	}

	public function delete($xid){
		$id = de($xid);

		$where = array(
			'id_dokter' => $id
		);
		
		$this->datadokter->delete($where);
		redirect('dokter');
	}
}
