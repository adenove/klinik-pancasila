<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if ($this->session->userdata('logged_in_user') == FALSE) { reject(); }

		$this->load->model('datapasien');
		$this->load->helper('url');
		$this->load->library('encryption');
		$this->load->model('datakunjungan');
	}

	public function index()
	{
		$data['title']="Pasien"; //title
		$data['data'] = $this->datapasien->daftar()->result();

		$data['page'] = "pasien"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	function get_kecamatan(){ // Ajak Kabupaten dan Kecamatan
        $id = $this->input->post('id');
        $data = $this->datapasien->get_kecamatan('id_kabupaten',$id)->result();
        echo json_encode($data);
    }

	public function input()
	{

		//Daftar Kabupaten
		// $dataKabupaten = $this->datapasien->daftar_kabupaten()->result();
		// foreach($dataKabupaten as $d){
		// 	$id_kabupaten[] = $d->id_kabupaten;
		// 	$nama_kabupaten[] = $d->nama_kabupaten;
		// }

		//Daftar Kecamatan
		// $dataKecamatan = $this->datapasien->daftar_kecamatan()->result();
		// foreach($dataKecamatan as $d){
		// 	$id_kecamatan[] = $d->id_kecamatan;
		// 	$nama_kecamatan[] = $d->nama_kecamatan;
		// }

		//Daftar Dokter
		$dataDokter = $this->datakunjungan->daftar_dokter()->result();
		foreach($dataDokter as $d){
			$id_dokter[] = $d->id_dokter;
			$nama_dokter[] = $d->nama_dokter;
		}

		$maxid=$this->datapasien->id_pasien();
		$noUrut = (int) substr($maxid,0,6);
		$noUrut++;
		$newID = "".sprintf("%06s",$noUrut);

		$maxid=$this->datapasien->nomor_rekam_medis();
		$noUrut = (int) substr($maxid,0,6);
		$noUrut++;
		$newNO = "".sprintf("%06s",$noUrut);

		$data['title']="Pasien"; //title

		$data['action'] = "pasien/create";

		$data['arrayIdKabupaten'] = [];
		$data['arrayNamaKabupaten'] = [];
		$data['arrayIdKecamatan'] = [];
		$data['arrayNamaKecamatan'] = [];

		$data['id_pasien'] = $newID;
		$data['nomor_rekam_medis'] = $newNO;
		$data['nama_pasien'] = "";
		$data['tanggal_lahir'] = "";
		$data['usia'] = "";
		$data['agama'] = "";
		$data['jenis_kelamin'] = "";
		$data['id_kabupaten'] = "";
		$data['id_kecamatan'] = "";
		$data['alamat'] = "";
		$data['status'] = "";
		$data['pendidikan'] = "";
		$data['pekerjaan'] = "";
		$data['jenis_pasien'] = "";

		$data['provinsi']  = "";
		$data['kabupaten'] = "";
		$data['kecamatan'] = "";
		$data['kelurahan'] = "";

		$data['nama_orangtua'] = "";

		$data['arrayIdDokter'] = $id_dokter;
		$data['arrayNamaDokter'] = $nama_dokter;

		$data['xid_kunjungan'] = "";
		$data['jenis_kunjungan'] = "";
		$data['keluhan'] = "";
		$data['id_pasien'] = "";
		$data['id_dokter'] = "";

		$data['page'] = "pasien_form"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function edit($xid)
	{
		$id = de($xid);

		// //Daftar Kabupaten
		// $dataKabupaten = $this->datapasien->daftar_kabupaten()->result();
		// foreach($dataKabupaten as $d){
		// 	$id_kabupaten[] = $d->id_kabupaten;
		// 	$nama_kabupaten[] = $d->nama_kabupaten;
		// }

		// //Daftar Kecamatan
		// $dataKecamatan = $this->datapasien->daftar_kecamatan()->result();
		// foreach($dataKecamatan as $d){
		// 	$id_kecamatan[] = $d->id_kecamatan;
		// 	$nama_kecamatan[] = $d->nama_kecamatan;
		// }

		//Daftar Dokter
		$dataDokter = $this->datakunjungan->daftar_dokter()->result();
		foreach($dataDokter as $d){
			$id_dokter[] = $d->id_dokter;
			$nama_dokter[] = $d->nama_dokter;
		}

		//Data Kunjunga
		$dataKunjungan = $this->datakunjungan->daftar('kunjungan.id_pasien',$id)->result();

		$data['title']="Pasien"; //title
		$e = $this->datapasien->edit('id_pasien',$id)->row();

		$data['action'] = "pasien/update";

		$data['arrayIdKabupaten'] = [];
		$data['arrayNamaKabupaten'] = [];
		$data['arrayIdKecamatan'] = [];
		$data['arrayNamaKecamatan'] = [];

		$data['id_pasien'] = $id;
		$data['nomor_rekam_medis'] = $e->nomor_rekam_medis;
		$data['nama_pasien'] = $e->nama_pasien;
		$data['tanggal_lahir'] = $e->tanggal_lahir;
		$data['usia'] = $e->usia;
		$data['agama'] = $e->agama;
		$data['jenis_kelamin'] = $e->jenis_kelamin;
		$data['id_kabupaten'] = "";
		$data['id_kecamatan'] = "";
		$data['alamat'] = $e->alamat;
		$data['status'] = $e->status;
		$data['pendidikan'] = $e->pendidikan;
		$data['pekerjaan'] = $e->pekerjaan;
		$data['jenis_pasien'] = $e->jenis_pasien;

		$data['provinsi'] = $e->provinsi;
		$data['kabupaten'] = $e->kabupaten;
		$data['kecamatan'] = $e->kecamatan;
		$data['kelurahan'] = $e->kelurahan;

		$data['nama_orangtua'] = $e->nama_orangtua;

		$data['arrayIdDokter'] = $id_dokter;
		$data['arrayNamaDokter'] = $nama_dokter;

		$data['jenis_kunjungan'] = $dataKunjungan[0]->jenis_kunjungan;
		$data['keluhan'] = $dataKunjungan[0]->keluhan;
		$data['id_dokter'] = $dataKunjungan[0]->id_dokter ;

		$data['page'] = "pasien_form"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function create()
	{
		date_default_timezone_set('Asia/Jakarta');
		$now = date('Y-m-d H:i:s');

		$bdate = date("Y-m-d",strtotime($this->input->post('tanggal_lahir')));
        $age = date_diff(date_create($bdate), date_create('now'));
        if($age->y > 0){
            $age = $age->y ." tahun";
        }else if($age->m > 0){
            $age = $age->m ." bulan" ;
        }else{
			$age = $age->d ." hari" ;
		}

		$maxid=$this->datapasien->id_pasien();
		$noUrut = (int) substr($maxid,0,6);
		$noUrut++;
		$newID = "".sprintf("%06s",$noUrut);

		$data = array(
			'id_pasien' 		=> $newID,
			'nomor_rekam_medis' => $this->input->post('nomor_rekam_medis'),
			'nama_pasien' 		=> $this->input->post('nama_pasien'),
			'tanggal_lahir' 	=> date("Y-m-d",strtotime($this->input->post('tanggal_lahir'))),
			'usia' 				=> $age,
			'agama' 			=> $this->input->post('agama'),
			'jenis_kelamin' 	=> $this->input->post('jenis_kelamin'),
			'alamat' 			=> $this->input->post('alamat'),
			'nama_orangtua'		=> $this->input->post('nama_orangtua'),
			'status' 			=> $this->input->post('status'),
			'pendidikan' 		=> $this->input->post('pendidikan'),
			'pekerjaan' 		=> $this->input->post('pekerjaan'),
			'jenis_pasien' 		=> $this->input->post('jenis_pasien'),
			'provinsi'			=> $this->input->post('provinsi'),
			'kabupaten'			=> $this->input->post('kabupaten'),
			'kecamatan'			=> $this->input->post('kecamatan'),
			'kelurahan'			=> $this->input->post('kelurahan')
		);

		$kunjungan = array(
			'tanggal_kunjungan' => $now,
			'jenis_kunjungan' => $this->input->post('jenis_kunjungan'),
			'keluhan' => $this->input->post('keluhan'),
			'id_pasien' => $newID,
			'id_dokter' => $this->input->post('id_dokter'),
			'kunjungan_status' => 'antre'
		);

		$this->datakunjungan->add($kunjungan);
		$this->datapasien->add($data);
		redirect('pasien');
	}

	public function update(){

		$id = $this->input->post('id'); //tidak di enkripsi karena ID berurutan

		//set time zone
		date_default_timezone_set('Asia/Jakarta');
		//set kunjungan hari ini
		$now = date('Y-m-d H:i:s');

		//set usia
		$bdate = date("Y-m-d",strtotime($this->input->post('tanggal_lahir')));
        $age = date_diff(date_create($bdate), date_create('now'));
        if($age->y > 0){
            $age = $age->y ." tahun";
        }else if($age->m > 0){
            $age = $age->m ." bulan" ;
        }else{
			$age = $age->d ." hari" ;
		}

		$data = array(
			'nomor_rekam_medis' => $this->input->post('nomor_rekam_medis'),
			'nama_pasien' 		=> $this->input->post('nama_pasien'),
			'tanggal_lahir' 	=> date("Y-m-d",strtotime($this->input->post('tanggal_lahir'))),
			'usia' 				=> $age,
			'agama' 			=> $this->input->post('agama'),
			'jenis_kelamin' 	=> $this->input->post('jenis_kelamin'),
			'alamat' 			=> $this->input->post('alamat'),
			'nama_orangtua'		=> $this->input->post('nama_orangtua'),
			'status' 			=> $this->input->post('status'),
			'pendidikan' 		=> $this->input->post('pendidikan'),
			'pekerjaan' 		=> $this->input->post('pekerjaan'),
			'jenis_pasien' 		=> $this->input->post('jenis_pasien'),
			'provinsi'			=> $this->input->post('provinsi'),
			'kabupaten'			=> $this->input->post('kabupaten'),
			'kecamatan'			=> $this->input->post('kecamatan'),
			'kelurahan'			=> $this->input->post('kelurahan')
		);

		$kunjungan = array(
			'jenis_kunjungan' => $this->input->post('jenis_kunjungan'),
			'keluhan' => $this->input->post('keluhan'),
			'id_dokter' => $this->input->post('id_dokter')
		);

		$where = array(
			'id_pasien' => $id
		);

		$this->datapasien->update($where,$data);
		$this->datakunjungan->update($where,$kunjungan);
		redirect('pasien');
	}

	public function delete($xid){
		$id = de($xid);

		$where = array(
			'id_pasien' => $id
		);

		$this->datapasien->delete($where);
		redirect('pasien');
	}

	public function kartu($xid){
		$id = de($xid);
		$data['kartu'] = $this->datapasien->get_pasien($id);
		$data['title'] = 'kartu';
		$data['page'] = "cetak"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}


	public function info($xid)
	{
		$id = de($xid);

		// //Daftar Kabupaten
		// $dataKabupaten = $this->datapasien->daftar_kabupaten()->result();
		// foreach($dataKabupaten as $d){
		// 	$id_kabupaten[] = $d->id_kabupaten;
		// 	$nama_kabupaten[] = $d->nama_kabupaten;
		// }

		// //Daftar Kecamatan
		// $dataKecamatan = $this->datapasien->daftar_kecamatan()->result();
		// foreach($dataKecamatan as $d){
		// 	$id_kecamatan[] = $d->id_kecamatan;
		// 	$nama_kecamatan[] = $d->nama_kecamatan;
		// }

		//Daftar Dokter
		$dataDokter = $this->datakunjungan->daftar_dokter()->result();
		foreach($dataDokter as $d){
			$id_dokter[] = $d->id_dokter;
			$nama_dokter[] = $d->nama_dokter;
		}

		//Data Kunjunga
		$dataKunjungan = $this->datakunjungan->daftar('kunjungan.id_pasien',$id)->result();

		$data['title']="Pasien"; //title
		$e = $this->datapasien->edit('id_pasien',$id)->row();

		$data['action'] = "pasien/update";

		$data['arrayIdKabupaten'] = [];
		$data['arrayNamaKabupaten'] = [];
		$data['arrayIdKecamatan'] = [];
		$data['arrayNamaKecamatan'] = [];

		$data['id_pasien'] = $id;
		$data['nomor_rekam_medis'] = $e->nomor_rekam_medis;
		$data['nama_pasien'] = $e->nama_pasien;
		$data['tanggal_lahir'] = $e->tanggal_lahir;
		$data['usia'] = $e->usia;
		$data['agama'] = $e->agama;
		$data['jenis_kelamin'] = $e->jenis_kelamin;
		$data['id_kabupaten'] = "";
		$data['id_kecamatan'] = "";
		$data['alamat'] = $e->alamat;
		$data['status'] = $e->status;
		$data['pendidikan'] = $e->pendidikan;
		$data['pekerjaan'] = $e->pekerjaan;
		$data['jenis_pasien'] = $e->jenis_pasien;

		$data['provinsi'] = $e->provinsi;
		$data['kabupaten'] = $e->kabupaten;
		$data['kecamatan'] = $e->kecamatan;

		$data['nama_orangtua'] = $e->nama_orangtua;

		$data['arrayIdDokter'] = $id_dokter;
		$data['arrayNamaDokter'] = $nama_dokter;

		$data['jenis_kunjungan'] = $dataKunjungan[0]->jenis_kunjungan;
		$data['keluhan'] = $dataKunjungan[0]->keluhan;
		$data['id_dokter'] = $dataKunjungan[0]->id_dokter ;

		// $data['page'] = "pasien_cetak"; //content page
		$this->load->view('pasien_cetak',$data); //layout
	}


}
