<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kabupaten extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if ($this->session->userdata('logged_in_user') == FALSE) { reject(); }

		$this->load->model('datakabupaten');
		$this->load->helper('url');
		$this->load->library('encryption');
	}

	public function index()
	{	
		$data['title']="Kabupaten"; //title
		$data['data'] = $this->datakabupaten->daftar()->result();
		
		$data['page'] = "kabupaten"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}
	
	public function create()
	{	
		$data = array(
			'nama_kabupaten' => $this->input->post('nama_kabupaten')
		);
		
		$this->datakabupaten->add($data);
		redirect('kabupaten');
	}

	public function delete($xid){
		$id = de($xid);

		$where = array(
			'id_kabupaten' => $id
		);
		
		$this->datakabupaten->delete($where);
		redirect('kabupaten');
	}
}
