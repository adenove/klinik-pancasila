<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if ($this->session->userdata('logged_in_user') == FALSE) { reject(); }

		$this->load->model('datakecamatan');
		$this->load->helper('url');
		$this->load->library('encryption');
	}

	public function index()
	{
		$data['title']="Kecamatan"; //title
		$data['data'] = $this->datakecamatan->daftar()->result();

		// $dataKabupaten = $this->datakecamatan->daftar_kabupaten()->result();
		// foreach($dataKabupaten as $d){
		// 	$id_kabupaten[] = $d->id_kabupaten;
		// 	$nama_kabupaten[] = $d->nama_kabupaten;
		// }
		$data['arrayIdKabupaten'] = [];
		$data['arrayNamaKabupaten'] = [];

		$data['page'] = "kecamatan"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function create()
	{
		$data = array(
			'id_kabupaten' => $this->input->post('id_kabupaten'),
			'nama_kecamatan' => $this->input->post('nama_kecamatan')
		);

		$this->datakecamatan->add($data);
		redirect('kecamatan');
	}


	public function delete($xid){
		$id = de($xid);

		$where = array(
			'id_kecamatan' => $id
		);

		$this->datakecamatan->delete($where);
		redirect('kecamatan');
	}
}
