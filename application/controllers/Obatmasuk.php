<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obatmasuk extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if ($this->session->userdata('logged_in_user') == FALSE) { reject(); }

		$this->load->model('dataobat');
		$this->load->model('dataobatmasuk');
		$this->load->helper('url');
	}

	public function index()
	{	
		$data['title']="Obat Masuk"; //title
		$data['data'] = $this->dataobatmasuk->daftar()->result();

		$data['page'] = "obatmasuk"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function input()
	{	
		$data['title']="Obat Masuk"; //title
		$data['data'] = $this->dataobatmasuk->daftar()->result();

		$dataObat = $this->dataobatmasuk->daftar_obat()->result();
		foreach($dataObat as $d){
			$id_obat[] = $d->id_obat;
			$nama_obat[] = $d->nama_obat;
		}
		$data['arrayIdObat'] = $id_obat;
		$data['arrayNamaObat'] = $nama_obat;

		$data['page'] = "obatmasuk_form"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}
	
	public function create()
	{	
		date_default_timezone_set('Asia/Jakarta');
		$now = date('Y-m-d H:i:s');

		$id_obat = $this->input->post('id_obat');
		$stok_baru = $this->input->post('jumlah_obat_masuk');
		$row = $this->dataobatmasuk->stok_obat($id_obat)->row();
		$stok_lama = $row->stok_obat;
		$total = $stok_lama + $stok_baru;

		$dataAdd = array(
			'id_obat' => $id_obat,
			'harga_beli' => $this->input->post('harga_beli'),
			'jumlah_obat_masuk' => $stok_baru,
			'tanggal_obat_masuk' => $now		
		);

		$this->dataobatmasuk->add($dataAdd);

		$dataUpdate = array(
			'stok_obat' => $total	
		);

		$this->dataobatmasuk->tambah_stok($id_obat,$dataUpdate);
		redirect('obatmasuk');
	}

	public function delete($xid){
		$id = de($xid);

		$x = $this->dataobatmasuk->filter('id_obat_masuk',$id)->row();
		$id_obat = $x->id_obat;
		$jumlah_hapus = $x->jumlah_obat_masuk;

		$row = $this->dataobatmasuk->stok_obat($id_obat)->row();
		$stok_lama = $row->stok_obat;
		$total = $stok_lama - $jumlah_hapus;

		$dataUpdate = array(
			'stok_obat' => $total	
		);
		$this->dataobatmasuk->tambah_stok($id_obat,$dataUpdate);

		$where = array(
			'id_obat_masuk' => $id
		);
		
		$this->dataobatmasuk->delete($where);
		redirect('obatmasuk');
	}
}
