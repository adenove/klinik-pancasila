<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kunjungan extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if ($this->session->userdata('logged_in_user') == FALSE) { reject(); }

		$this->load->model('datakunjungan');
		$this->load->helper('url');
		$this->load->library('encryption');
	}

	public function index()
	{
		$data['title']="Kunjungan"; //title
		$data['data'] = $this->datakunjungan->daftar('kunjungan_status','antre')->result();

		$data['page'] = "kunjungan"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function input($xid)
	{
		$id = de($xid);
		//Daftar Pasien
		$dataPasien = $this->datakunjungan->daftar_pasien()->result();
		foreach($dataPasien as $d){
			if($d->id_pasien == $id){
				$id_pasien[] = $d->id_pasien;
				//$nama_pasien[] = $d->nama_pasien.' <i>('.$d->alamat.', '.$d->nama_kecamatan.', '.$d->nama_kabupaten.')</i>';
				$nama_pasien[] = $d->nama_pasien.' <i>( No. RM : '.$d->id_pasien.')</i>';
			}
		}


		//Daftar Dokter
		$dataDokter = $this->datakunjungan->daftar_dokter()->result();
		foreach($dataDokter as $d){
			$id_dokter[] = $d->id_dokter;
			$nama_dokter[] = $d->nama_dokter;
		}

		$data['title']="Kunjungan"; //title

		$data['action'] = "kunjungan/create";

		$data['arrayIdPasien'] = $id_pasien;
		$data['arrayNamaPasien'] = $nama_pasien;
		$data['arrayIdDokter'] = $id_dokter;
		$data['arrayNamaDokter'] = $nama_dokter;

		$data['xid_kunjungan'] = "";
		$data['jenis_kunjungan'] = "";
		$data['keluhan'] = "";
		$data['id_pasien'] = "";
		$data['id_dokter'] = "";

		$data['page'] = "kunjungan_form"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function edit($xid)
	{
		$id = de($xid);

		//Daftar Pasien
		$dataPasien = $this->datakunjungan->daftar_pasien()->result();
		foreach($dataPasien as $d){
			$id_pasien[] = $d->id_pasien;
			$nama_pasien[] = $d->nama_pasien.' <i>('.$d->alamat.', '.$d->kecamatan.', '.$d->kabupaten.', '.$d->provinsi.')</i>';
		}

		//Daftar Dokter
		$dataDokter = $this->datakunjungan->daftar_dokter()->result();
		foreach($dataDokter as $d){
			$id_dokter[] = $d->id_dokter;
			$nama_dokter[] = $d->nama_dokter;
		}

		$data['title']="Kunjungan"; //title

		$data['action'] = "kunjungan/create";

		$data['arrayIdPasien'] = $id_pasien;
		$data['arrayNamaPasien'] = $nama_pasien;
		$data['arrayIdDokter'] = $id_dokter;
		$data['arrayNamaDokter'] = $nama_dokter;

		$e = $this->datakunjungan->edit('id_kunjungan',$id)->row();
		$data['xid_kunjungan'] = $xid;
		$data['jenis_kunjungan'] = $e->jenis_kunjungan;
		$data['keluhan'] = $e->keluhan;
		$data['id_pasien'] = $e->id_pasien;
		$data['id_dokter'] = $e->id_dokter;

		$data['page'] = "kunjungan_form"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function create()
	{
		date_default_timezone_set('Asia/Jakarta');
		$now = date('Y-m-d H:i:s');
		$data = array(
			'tanggal_kunjungan' => $now,
			'jenis_kunjungan' => $this->input->post('jenis_kunjungan'),
			'keluhan' => $this->input->post('keluhan'),
			'id_pasien' => $this->input->post('id_pasien'),
			'id_dokter' => $this->input->post('id_dokter'),
			'kunjungan_status' => 'antre'
		);

		$this->datakunjungan->add($data);
		redirect('kunjungan');
	}

	public function delete($xid){
		$id = de($xid);

		$where = array(
			'id_kunjungan' => $id
		);

		$this->datakunjungan->delete($where);
		redirect('kunjungan');
	}
}
