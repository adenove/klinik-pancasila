<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if ($this->session->userdata('logged_in_user') == FALSE) { reject(); }

		$this->load->model('datauser');
		$this->load->helper('url');
		$this->load->library('encryption');
		// $this->encryption->encrypt();
		// $this->encryption->decrypt();
	}

	public function index()
	{	
		$data['title']="Petugas"; //title
		$data['data'] = $this->datauser->daftar()->result();
		
		$data['page'] = "user"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function input()
	{	
		$data['title']="Petugas"; //title

		$data['action'] = "user/create";

		$data['xid_user'] = "";
		$data['nama_user'] = "";
		$data['username_user'] = "";
		$data['password_user'] = "";
		$data['level'] = "";

		$data['page'] = "user_form"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function edit($xid)
	{	
		$id = de($xid);

		$data['title']="Petugas"; //title
		$e = $this->datauser->edit('id_user',$id)->row();

		$data['action'] = "user/update";

		$data['xid_user'] = $xid;
		$data['nama_user'] = $e->nama_user;
		$data['username_user'] = $e->username_user;
		$data['password_user'] = $e->password_user;
		$data['level'] = $e->level;

		$data['page'] = "user_form"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}
	
	public function create()
	{	
		$data = array(
			'nama_user' => $this->input->post('nama'),
			'username_user' => $this->input->post('username'),
			'password_user' => md5($this->input->post('password')),
			'level' => $this->input->post('level')
		);
		
		$this->datauser->add($data);
		redirect('user');
	}

	public function update(){

		$xid = $this->input->post('id');
		$id = de($xid);
		
		$data = array(
			'nama_user' => $this->input->post('nama'),
			'username_user' => $this->input->post('username'),
			'password_user' => md5($this->input->post('password')),
			'level' => $this->input->post('level')
		);
		
		$where = array(
			'id_user' => $id
		);
		
		$this->datauser->update($where,$data);
		redirect('user');
	}

	public function delete($xid){
		$id = de($xid);

		$where = array(
			'id_user' => $id
		);
		
		$this->datauser->delete($where);
		redirect('user');
	}
}
