<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if ($this->session->userdata('logged_in_user') == FALSE) { reject(); }

		$this->load->model('datalaporan');
		$this->load->model('dataresep');
		$this->load->model('datapembayaran');
		$this->load->helper('url');
		$this->load->library('encryption');
	}

	public function obat()
	{
		$data['title']="Laporan Stok Obat"; //title
		$data['data'] = $this->datalaporan->daftar_obat()->result();

		$data['page'] = "laporan_obat"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function kunjungan()
	{
		if($this->input->post('awal')!= "" && $this->input->post('akhir') != ""){
			$data['title']="Laporan Kunjungan(". $this->input->post('awal') ." / ".$this->input->post('akhir')." )" ; //title
		}else{
			$data['title']="Laporan Kunjungan";
		}
		// $data['title']="Laporan Kunjungan"; //title
		$data['action'] = "laporan/kunjungan";
		$x = date("Y-m-d",strtotime($this->input->post('awal')));
		$y = date("Y-m-d",strtotime($this->input->post('akhir')));

		$data['data'] = $this->datalaporan->daftar_kunjungan($x,$y)->result();

		$data['page'] = "laporan_kunjungan"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function pembayaran()
	{
		if($this->input->post('awal')!= "" && $this->input->post('akhir') != ""){
			$data['title']="Laporan Pembayaran(". $this->input->post('awal') ." / ".$this->input->post('akhir')." )" ; //title
		}else{
			$data['title']="Laporan Pembayaran";
		}
		// $data['title']="Laporan Pembayaran"; //title
		$data['action'] = "laporan/pembayaran";
		$x = date("Y-m-d",strtotime($this->input->post('awal')));
		$y = date("Y-m-d",strtotime($this->input->post('akhir')));
		$data['data'] = $this->datalaporan->daftar_pembayaran($x,$y)->result();

		$data['page'] = "laporan_pembayaran"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function riwayat(){
		if($this->input->post('awal')!= "" && $this->input->post('akhir') != ""){
			$data['title']="Laporan Riwayat(". $this->input->post('awal') ." / ".$this->input->post('akhir')." )" ; //title
		}else{
			$data['title']="Laporan Riwayat";
		}

		$data['action'] = "laporan/riwayat";
		$x = date("Y-m-d",strtotime($this->input->post('awal')));
		$y = date("Y-m-d",strtotime($this->input->post('akhir')));
		$data['all'] = $this->datalaporan->daftar_periksa($x,$y)->result();
		$data['page'] = "laporan_riwayat"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function riwayat_print($xid){
		$id = de($xid);
		$data['data'] = $this->datalaporan->daftar_riwayat($id)->result();
		$data['dataResep'] = $this->dataresep->daftar('kunjungan.id_kunjungan',$id)->result(); //data obat pada resep
		$data['dataBiayaLain'] = $this->datapembayaran->daftar_biaya_lain('kunjungan.id_kunjungan',$id)->result(); //data biaya lainnya
		$data['page'] = "invoice"; //content page

		$this->load->view('print_riwayat',$data);
	}

}
