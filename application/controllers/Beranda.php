<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if ($this->session->userdata('logged_in_user') == FALSE) { reject(); }

		$this->load->model('databeranda');
		$this->load->helper('url');
		$this->load->library('encryption');
	}

	public function index()
	{
		$data['title']="Beranda Admin"; //title
		$x = date("Y-m-d");
		$isdate = date("Y-m-d",strtotime($x));
		$data['jumlahDokter'] = $this->databeranda->jumlah_dokter()->num_rows();
		$data['jumlahPasien'] = $this->databeranda->jumlah_pasien($isdate)->num_rows();
		$data['jumlahKunjungan'] = $this->databeranda->jumlah_kunjungan()->num_rows();
		$data['jumlahPetugas'] = $this->databeranda->jumlah_petugas()->num_rows();
		$data['page'] = "beranda"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}
}
