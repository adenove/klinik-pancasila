<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periksa extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if ($this->session->userdata('logged_in_user') == FALSE) { reject(); }

		$this->load->model('datakunjungan');
		$this->load->model('dataresep');
		$this->load->model('datadiagnosa');
		$this->load->model('dataobat');
		$this->load->helper('url');
		$this->load->library('encryption');
	}

	public function index()
	{
		//Daftar Pasien
		$dataPasien = $this->datakunjungan->daftar_pasien()->result();
		foreach($dataPasien as $d){
			$id_pasien[] = $d->id_pasien;
			$nama_pasien[] = $d->nama_pasien.' <i>('.$d->alamat.', '.$d->kecamatan.', '.$d->kabupaten.', '.$d->provinsi.')</i>';
		}

		//Daftar Dokter
		$dataDokter = $this->datakunjungan->daftar_dokter()->result();
		foreach($dataDokter as $d){
			$id_dokter[] = $d->id_dokter;
			$nama_dokter[] = $d->nama_dokter;
		}

		$data['title']="Periksa"; //title
		$data['data'] = $this->datakunjungan->daftar('kunjungan_status','antre')->result();

		$data['arrayIdPasien'] = $id_pasien;
		$data['arrayNamaPasien'] = $nama_pasien;
		$data['arrayIdDokter'] = $id_dokter;
		$data['arrayNamaDokter'] = $nama_dokter;

		$data['page'] = "antrean"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function daftar_selesai_periksa()
	{
		$data['title']="Selesai Periksa"; //title
		$data['data'] = $this->dataresep->daftar_periksa()->result();

		$data['page'] = "periksa"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function resep($xid)
	{
		$id = de($xid);

		$data['title']="Resep"; //title
		$data['dataKunjungan'] = $this->datakunjungan->daftar('kunjungan.id_kunjungan',$id)->result(); //data kunjungan
		$data['dataResep'] = $this->dataresep->daftar('kunjungan.id_kunjungan',$id)->result(); //data obat pada resep

		$dataObat = $this->dataobat->daftar()->result();
		foreach($dataObat as $d){
			$id_obat[] = $d->id_obat;
			$nama_obat[] = $d->nama_obat;
		}

		$diagnosa = $this->datadiagnosa->get()->result();
		foreach( $diagnosa as $diag ){
			$id_diagnosa[] = $diag->icd;
			$icd_diagnosa[] = $diag->diagnosa;
		}

		$id = de($xid);

		$digits = 5;

		$data['id_icd'] = "";
		$data['diagnosa'] = "";

		$data['icd'] = "";
		$data['resep'] = $id;
		$data['arrayIdObat'] = $id_obat;
		$data['arrayNamaObat'] = $nama_obat;
		$data['id_kunjungan'] = $xid;

		$data['idDiagnosa'] = $id_diagnosa;
		$data['icd_Diagnosa'] = $icd_diagnosa;

		$data['stockobat'] = $this->dataobat->daftar()->result();

		$data['page'] = "resep"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function create_resep()
	{
		$xid = $this->input->post('id_kunjungan');
		$id = de($xid);
		$id_obat = $this->input->post('id_obat');
		$stok_keluar = $this->input->post('jumlah_obat_keluar');

		$row = $this->dataresep->stok_obat($id_obat)->row();
		$stok_lama = $row->stok_obat;
		$total = $stok_lama - $stok_keluar;

		$dataAdd = array(
			'id_kunjungan' => $id,
			'id_obat' => $id_obat,
			'jumlah_obat_keluar' => $stok_keluar,
			'keterangan' => $this->input->post('keterangan')
		);

		$this->dataresep->add($dataAdd);

		$dataUpdate = array(
			'stok_obat' => $total
		);

		$this->dataresep->kurangi_stok($id_obat,$dataUpdate);
		redirect('periksa/resep/'.$xid);
	}

	public function create_periksa()
	{
		$xid = $this->input->post('id_kunjungan');
		$id = de($xid);

		$dataAdd = array(
			'id_kunjungan' => $id,
			'icd' => $this->input->post('icd'),
			'diagnosa' => $this->input->post('diagnosa'),
			'tindakan' => $this->input->post('tindakan')
		);

		$this->dataresep->add_periksa($dataAdd);

		//Update kunjungan status
		$dataUpdate = array(
			'kunjungan_status' => 'resep'
		);

		$this->dataresep->update_status($id,$dataUpdate);
		redirect('periksa');
	}

	public function delete($resep,$xid)
	{
		$id = de($xid);
		$this->dataresep->obat_by_id($id);
		$this->dataresep->delete($id);
		redirect(base_url().'periksa/resep/'.$resep);
	}

	public function edit()
	{
		$this->dataresep->get_edit();
	}


	public function edit_resep()
	{
		$xid = $this->input->post('id_kunjungan');
		$id = de($xid);

		$this->dataresep->edit_resep();


	}


}
