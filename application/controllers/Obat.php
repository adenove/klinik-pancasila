<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Obat extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if ($this->session->userdata('logged_in_user') == FALSE) { reject(); }

		$this->load->model('dataobat');
		$this->load->helper('url');
	}

	public function index()
	{
		$data['title']="Obat"; //title
		$data['data'] = $this->dataobat->daftar()->result();

		$data['page'] = "obat"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function input()
	{
		$data['title']="Obat"; //title

		$data['action'] = "obat/create";

		$data['xid_obat'] = "";
		$data['nama_obat'] = "";
		$data['harga_jual'] = "";

		$data['page'] = "obat_form"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function create()
	{
		$data = array(
			'nama_obat' => $this->input->post('nama'),
			'harga_jual' => $this->input->post('harga')
		);

		$this->dataobat->add($data);
		redirect('obat');
	}

	public function edit($xid)
	{
		$id = de($xid);

		$data['title']="Obat"; //title
		$data['action'] = "obat/update";

		$e = $this->dataobat->edit('id_obat',$id)->row();
		$data['xid_obat'] = $xid;
		$data['nama_obat'] = $e->nama_obat;
		$data['harga_jual'] = $e->harga_jual;

		$data['page'] = "obat_form"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function update(){

		$xid = $this->input->post('id');
		$id = de($xid);

		$data = array(
			'nama_obat' => $this->input->post('nama'),
			'harga_jual' => $this->input->post('harga')
		);

		$where = array(
			'id_obat' => $id
		);

		$this->dataobat->update($where,$data);
		redirect('obat');
	}

	public function delete($xid){
		$id = de($xid);

		$where = array(
			'id_obat' => $id
		);

		$this->dataobat->delete($where);
		redirect('obat');
	}

	public function restore($id,$restore){
		echo  'id_obat = '.$id;
		echo '<br>';
		echo 'penambahan = ';

	}
}
