<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diagnosa extends CI_Controller {

	public function __construct(){
		parent::__construct();

        if ($this->session->userdata('logged_in_user') == FALSE) { reject(); }
        $this->load->model('datadiagnosa');

	}

	public function index()
	{
        $data['title'] = "icd & diagnosa";
        $data['data'] = $this->datadiagnosa->get()->result();
        $data['page'] = "diagnosa"; //content page
		$this->load->view('layout_dashboard',$data); //layout
    }

    public function create()
    {
        $icd_diagnosa = array(
                                'icd'       => $this->input->post('icd'),
                                'diagnosa'  => $this->input->post('diagnosa')
                                );

        $this->datadiagnosa->add($icd_diagnosa);
        redirect(base_url('/diagnosa'));
    }

    public function delete($xid)
    {
        $id = de($xid);
        $where = array ('id' => $id );
        $this->datadiagnosa->delete($where);
        redirect(base_url('/diagnosa'));
    }

    public function update()
    {
        $edit = array(  'icd'       => $this->input->post('icd'),
                        'diagnosa'  => $this->input->post('diagnosa')  );

        $where = array( 'id' => $this->input->post('id') );

        $this->datadiagnosa->update($where,$edit);
        redirect(base_url('/diagnosa'));

    }

    public function edit($id)
    {
        $where = array('id'=>$id);
        $data = $this->datadiagnosa->get_where($where)->result();
        echo json_encode($data);
    }

    public function getdiagnosa()
    {
        $icd = $this->input->post('icd');
        $where = array('icd' => $icd );
        $data = $this->datadiagnosa->get_where($where)->result();
        echo json_encode($data);

    }

}
