<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if ($this->session->userdata('logged_in_user') == FALSE) { reject(); }

	}

	public function index()
	{
		$data['title'] = "Backup"; //title
		$data['page'] = "table"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function backup()
	{
		$data['title'] = "Backup"; //title
		$data['page'] = "backup"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function restore()
	{
		$data['title'] = "Restore"; //title
		$data['action'] = "data/upload"; //action
		$data['page'] = "restore"; //content page
		$data['errormsg'] = "";
		$this->load->view('layout_dashboard',$data); //layout
	}


	public function backupdb(){

	   // Load the DB utility class
		   $this->load->dbutil();

		   $prefs = array(
				   'format'      => 'sql',
				   'filename'    => 'backup-on-'.date("Y-m-d-H-i-s").'.sql'
				 );


		   $backup =& $this->dbutil->backup($prefs);

		   $db_name = 'backup-on-'. date("Y-m-d-H-i-s") .'.sql';
		   $save = FCPATH.'/'.$db_name;

		   $this->load->helper('file');
		//    write_file($save, $backup);

		   $this->load->helper('download');
		   force_download($db_name, $backup);
   }

   public function upload()
   {


		$tbl = $this->db->database;
		$usr = $this->db->username;
		$host = $this->db->hostname;
		$pass = $this->db->password;

		$mysqli = new mysqli($host, $usr,$pass, $tbl);
		$mysqli->query('SET foreign_key_checks = 0');
		if ($result = $mysqli->query("SHOW TABLES"))
		{
			while($row = $result->fetch_array(MYSQLI_NUM))
			{
				$mysqli->query('DROP TABLE IF EXISTS '.$row[0]);
			}
		}

		$mysqli->query('SET foreign_key_checks = 1');
		$mysqli->close();

		// restore
		$isi_file = file_get_contents($_FILES['profile_image']['tmp_name']);
		$string_query = rtrim( $isi_file, "\n;" );
  		$array_query = explode(";", $string_query);

		foreach ($array_query as $query) {
			$this->db->query($query);
		}

		$data['errormsg'] = "Data Berhasil di Restore";
		$data['title'] = "Restore";
		$data['action'] = "data/upload"; //action
		$data['page'] = "restore"; //content page
		$this->load->view('layout_dashboard',$data); //layout

   }


}
