<?php
class Auth extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('dataauth');
    }

    function index(){
        $data['notif'] = ""; //notifikasi javasctipt disable
        $this->load->view('layout_auth',$data);
    }

    function check(){
        $username = htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
        $password = htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);

        $cek_user = $this->dataauth->auth_user($username,$password);
        $cek_dokter = $this->dataauth->auth_dokter($username,$password);

        if($cek_user->num_rows() == 1){ //jika login sebagai Petugas
            foreach($cek_user->result() as $data){
                $user_data = array(
                    'id' => $data->id_user,
                    'nama' => $data->nama_user,
                    'username' => $data->username_user,
                    'level' => $data->level,
                    'logged_in_user' => TRUE
                );
                $this->session->set_userdata($user_data);
            }
            if ($this->session->userdata('level') == '1') { redirect('beranda'); }
            else if ($this->session->userdata('level') == '2') { redirect('pasien'); }
            else if ($this->session->userdata('level') == '3') { redirect('obatmasuk'); }
            else if ($this->session->userdata('level') == '4') { redirect('pembayaran'); }
            else { redirect('auth/logout');}
        }

        elseif($cek_dokter->num_rows() == 1){ //jika login sebagai Dokter
            foreach($cek_dokter->result() as $data){
                $user_data = array(
                    'id' => $data->id_dokter,
                    'nama' => $data->nama_dokter,
                    'username' => $data->username_dokter,
                    'level' => 'Dokter',
                    'logged_in_user' => TRUE
                );
                $this->session->set_userdata($user_data);
            }
            redirect('periksa');
        }

        else {
            $data['notif'] = "notify"; //notifikasi javasctipt enable
            $this->load->view('layout_auth',$data);
        }
    }

    function logout()
    {
        // $a=array('logged_in_user');
        // $this->session->unset_userdata($a);
        $this->session->sess_destroy();
        redirect('auth');
    }
}
