<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if ($this->session->userdata('logged_in_user') == FALSE) { reject(); }

		$this->load->model('datapembayaran');
		$this->load->model('datakunjungan');
		$this->load->model('dataresep');
		$this->load->model('dataobat');
		$this->load->helper('url');
		$this->load->library('encryption');
	}

	public function index()
	{
		$data['title']="Pembayaran (Belum Lunas)"; //title
		$data['data'] = $this->datakunjungan->daftar('kunjungan_status','resep')->result();

		$data['page'] = "pembayaran"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function lunas()
	{
		$data['title']="Pembayaran (Lunas)"; //title
		$data['data'] = $this->datakunjungan->daftar('kunjungan_status','lunas')->result();

		$data['page'] = "pembayaran"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function invoice($xid)
	{
		$id = de($xid);

		$data['title']="Resep"; //title
		$data['dataKunjungan'] = $this->datakunjungan->daftar('kunjungan.id_kunjungan',$id)->result(); //data kunjungan
		$data['dataResep'] = $this->dataresep->daftar('kunjungan.id_kunjungan',$id)->result(); //data obat pada resep
		$data['dataBiayaLain'] = $this->datapembayaran->daftar_biaya_lain('kunjungan.id_kunjungan',$id)->result(); //data biaya lainnya

		$data['id_kunjungan'] = $xid;

		$data['page'] = "invoice"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function create_biaya_lain()
	{
		$xid = $this->input->post('id_kunjungan');
		$id = de($xid);

		$dataAdd = array(
			'id_kunjungan' => $id,
			'jenis_layanan' => $this->input->post('jenis_layanan'),
			'biaya_layanan' => $this->input->post('biaya_layanan'),
			'jumlah_layanan' => $this->input->post('jumlah_layanan')
		);

		$this->datapembayaran->addBiayaLain($dataAdd);
		redirect('pembayaran/invoice/'.$xid);
	}

	public function create()
	{
		$xid = $this->input->post('id_kunjungan');
		$id = de($xid);

		date_default_timezone_set('Asia/Jakarta');
		$now = date('Y-m-d H:i:s');

		$dataAdd = array(
			'id_kunjungan' => $id,
			'tanggal_pembayaran' => $now,
			'total_pembayaran' => $this->input->post('total_pembayaran')
		);

		$this->datapembayaran->add($dataAdd);

		$dataUpdate = array(
			'kunjungan_status' => 'lunas'
		);

		$this->datapembayaran->update_status($id,$dataUpdate);
		redirect('pembayaran/lunas');
	}

	public function print($xid){
		$id = de($xid);

		$data['title']="Resep"; //title
		$data['dataKunjungan'] = $this->datakunjungan->daftar('kunjungan.id_kunjungan',$id)->result(); //data kunjungan
		$data['dataResep'] = $this->dataresep->daftar('kunjungan.id_kunjungan',$id)->result(); //data obat pada resep
		$data['dataBiayaLain'] = $this->datapembayaran->daftar_biaya_lain('kunjungan.id_kunjungan',$id)->result(); //data biaya lainnya

		$data['id_kunjungan'] = $xid;

		$data['page'] = "invoice"; //content page

		$this->load->view('print_invoice',$data);
	}

}
