<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['title'] = "Input Petugas"; //title
		$data['page'] = "form"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function form()
	{
		$data['title'] = "Input Petugas"; //title
		$data['page'] = "form"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function table()
	{
		$data['title'] = "Daftar Petugas"; //title
		$data['page'] = "table"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function auth()
	{
		$data['title'] = "Login"; //title
		$this->load->view('layout_auth',$data); //layout
	}
}
